onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib pmem_opt

do {wave.do}

view wave
view structure
view signals

do {pmem.udo}

run -all

quit -force
