`timescale 1ns / 1ps
`include "defines_inc.sv"

module imm_gen(
    input                   clk,
    input                   async_rst_n,
    input                   sync_rst,
    
    input   [31:0]          inst,
    
    output  [`XLEN-1:0]     imm
    );
    
// local copy of output ports
logic [`XLEN-1:0] imm_local;
logic [`XLEN-1:0] imm_local_d;
    
// internal signals
logic [6:0] opcode;

// opcode is the 7 bit LSB of instruction
assign opcode = inst[6:0];

//////////////////////////////////////////////////////////////////
//  Instruction Frmt    |   Opcode
//  ------------------------------------------
//      I               |  0x3 or x13 or x67
//      R               |  0x33
//      S               |  0x23
//      SB              |  0x63
//      U               |  0x37 or 0x17
//      UJ              |  0x6F or 0x67
//
//    Instruction formats are mapped to imm generation format 
//    according to Figure 2.4 of RISC-V spec v2.2
//////////////////////////////////////////////////////////////////
always_ff @ ( posedge clk ) 
    if ( ~async_rst_n ) 
        imm_local <= 0;
    else if ( sync_rst ) 
        imm_local <= 0;
    else if ( opcode == 7'h3 || opcode == 7'h13 ||  opcode == 7'h67 ) // I
        imm_local <= signed'(inst[31:20]);
    else if ( opcode == 7'h23 ) // S
        imm_local <= signed'({inst[31:25], inst[11:7]});
    else if ( opcode == 7'h63 ) // SB
        imm_local <= signed'( {inst[31],inst[7],inst[30:25],inst[11:8],1'b0} );
    else if ( opcode == 7'h37 || opcode == 7'h17 ) // U
        imm_local <= signed'( {inst[31:12],12'b0} );
    else if ( opcode == 7'h6F ) // UJ
        imm_local <= signed'( {inst[31],inst[19:12],inst[20],inst[30:21],1'b0} );
    else // R or undefined opcode
        imm_local <= 0;

// ONe more clock latency
always_ff @ ( posedge clk )
    if ( ~async_rst_n ) 
        imm_local_d <= 0;
    else if ( sync_rst ) 
        imm_local_d <= 0;
    else
        imm_local_d <= imm_local;
        
// connect local to output port
assign imm = imm_local_d;
  
endmodule