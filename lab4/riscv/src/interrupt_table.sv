`timescale 1ns / 1ps

`include "defines_inc.sv"

module interrupt_table(
    input   [7:0]       interrupt_in,
    output  [`ALEN-1:0] interrupt_serv_routine_addr
    );

logic [`ALEN-1:0] out;

always_comb begin    
    case ( interrupt_in ) 
        8'b00000001: out <= `INTRPT_SR_ADDR_1;
        8'b00000010: out <= `INTRPT_SR_ADDR_2;
        8'b00000100: out <= `INTRPT_SR_ADDR_3;
        8'b00001000: out <= `INTRPT_SR_ADDR_4;
        8'b00010000: out <= `INTRPT_SR_ADDR_5;
        8'b00100000: out <= `INTRPT_SR_ADDR_6;
        8'b01000000: out <= `INTRPT_SR_ADDR_7;
        8'b10000000: out <= `INTRPT_SR_ADDR_8;
        default: out <= 0;
    endcase
end

assign interrupt_serv_routine_addr = out;
    
endmodule
