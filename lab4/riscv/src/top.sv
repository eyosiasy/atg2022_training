`timescale 1ns / 1ps

`include "defines_inc.sv"

module top(
    input           clk,
    input           sync_rst,
    input           async_rst_n,
    
    input           uart_rx,
    output          uart_tx,
    output          loading_program,
    
    input   [31:0]  port_in,
    output  [31:0]  port_out,
    
    input   [7:0]   interrupt_in
    );
    
 localparam NOP = 32'h13; //ADDI x0,x0,0

 // Core states
 localparam RUNNING = 0;
 localparam LOADING_PROGRAM = 1;
  
 // Program counter
 logic      [`ALEN-1:0] pc;
 logic      [`ALEN-1:0] pc_next;
 logic      [`ALEN-1:0] pc_dline[8];
 logic                  odd_cycle; //0 for even cycle, 1 for odd cycle

 // ALU Ports
 logic      [3:0]       alu_op;
 logic      [`XLEN-1:0] alu_in0;
 logic      [`XLEN-1:0] alu_in1;
 logic      [`XLEN-1:0] alu_in0_pipe_fwd;
 logic      [`XLEN-1:0] alu_in1_pipe_fwd;
 logic      [`XLEN-1:0] alu_out; 
 logic      [`XLEN-1:0] alu_out_dline[4];    
 logic      [7:0]       alu_flag;
 
 // Control Unit Output Ports
 logic      [2:0]       branch_mode;
 logic      [2:0]       branch_mode_dline[2];
 logic                  alu_in0_sel;
 logic                  alu_in1_sel;
 logic                  data_mem_wr_en;
 logic                  data_mem_wr_en_dline[2];
 logic                  data_mem_rd_en;
 logic                  dest_reg_wr_en;
 logic                  dest_reg_wr_en_dline[4];
 logic      [1:0]       dest_reg_wr_data_sel;
 logic      [1:0]       dest_reg_wr_data_sel_dline[4];
 logic      [2:0]       data_mem_rd_sign_ext_mode;
 logic      [2:0]       data_mem_rd_sign_ext_mode_dline[2];
 logic      [6:0]       opcode;
 logic      [6:0]       opcode_dline[6];
 
 // Program Memory ports (R only from program, W only from outside world via UART)
 logic      [31:0]          instruction_rd;
 logic      [31:0]          instruction_rd_dline[2];
 logic      [31:0]          instruction;
 
 logic                      prog_mem_wr_en;
 logic      [`ALEN-1:0]     prog_mem_addr;
 logic      [`XLEN-1:0]     prog_mem_wr_data;
 logic      [`XLEN-1:0]     prog_mem_rd_data;
 
 // Data Memory ports (RW from program, not accessible to the ouside world)
 logic      [`ALEN-1:0]     data_mem_addr;
 logic                      data_mem_wr_data_en;
 logic      [31:0]          data_mem_wr_data;
 logic      [31:0]          data_mem_rd_data;
 logic      [31:0]          data_mem_rd_data_sign_ext;
 
 // Registers
 logic      [4:0]           src0_reg_addr;
 logic      [4:0]           src1_reg_addr;
 logic      [4:0]           dest_reg_addr;
 logic      [4:0]           src0_reg_addr_dline[2];
 logic      [4:0]           src1_reg_addr_dline[2];
 logic      [4:0]           dest_reg_addr_dline[6];
 logic      [`XLEN-1:0]     src0_reg_out;
 logic      [`XLEN-1:0]     src1_reg_out;
 logic      [`XLEN-1:0]     src1_reg_out_dline[2];    
 logic      [`XLEN-1:0]     dest_reg_in;
 
 // Immediate generation
 logic      [31:0]          imm;
 logic      [31:0]          imm_dline[4];
 
 // Branch and pipeline hazard
 logic                      branch;
 logic                      branch_dline[2];
 logic                      branch_stall;
 logic      [`ALEN-1:0]     pc_post_branch;
 
 // Data hazard
 logic                      stall;
 
 // UART
 logic                      start_programming_pulse;
 logic                      end_programming_pulse;
 logic                      state;
 logic                      uart_prog_mem_sync_rst;
 logic      [`ALEN-1:0]     uart_prog_mem_wr_addr;
 logic      [31:0]          uart_prog_mem_wr_data;
 logic                      uart_prog_mem_wr_en;
 
 // Interrupts
 logic                      interrupt_raised;
 logic      [`ALEN-1:0]     interrupt_serv_routine_addr;
 
 // Core state update
 // To load PMEM, the host sends a predefined sequence of bytes to UART-bridge. When the sequence is correctly received
 // the UART bridge indicates to the core that it is about to start programming by raising start_programming_pulse at least 
 // one cycle. When this pulse is raised, the top code enters "LOADING_PROGRAM" mode and stays there until UART bridge
 // asserts end_programming_pulse for at least one clock cycle.  
 always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) 
        state <= RUNNING;
    else if ( sync_rst ) 
        state <= RUNNING;
    else if ( start_programming_pulse & ( state == RUNNING ) )
        state <= LOADING_PROGRAM;
    else if ( end_programming_pulse   & ( state == LOADING_PROGRAM ) )
        state <= RUNNING;
    else
        state <= state;
 
 assign loading_program = ( state == LOADING_PROGRAM );
     
 // PC Logic 
 always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
        odd_cycle <= 0;
        pc <= `PC_INIT_ADDR;
    end
    else if ( sync_rst ) begin
        odd_cycle <= 0;
        pc <= `PC_INIT_ADDR;
    end
    else if ( end_programming_pulse ) begin
        odd_cycle <= 0;
        pc <= `PC_INIT_ADDR;
    end
    else begin
        odd_cycle <= ~odd_cycle;
        
        if ( odd_cycle )
            pc <= prog_mem_addr + 4;
    end
 
 // Program Loading Logic
 assign prog_mem_wr_en    = ( state == LOADING_PROGRAM ) & uart_prog_mem_wr_en;
 
 assign prog_mem_addr     = ( state == LOADING_PROGRAM ) ? uart_prog_mem_wr_addr : 
                            ( interrupt_raised )         ? interrupt_serv_routine_addr :
                            ( opcode_dline[1] == 7'h67 ) ? alu_out :
                            ( branch )                   ? pc_dline[5] + signed'(imm_dline[1]) :
                            ( stall )                    ? pc_dline[1] :  pc;
 
 assign prog_mem_wr_data  = ( state == LOADING_PROGRAM ) ? uart_prog_mem_wr_data : 0;
 
 // Program mem reading output (part of Data Hazard Stall Logic )
 assign instruction = ( stall ) ? NOP : instruction_rd; 

 // Register Inputs
 assign src0_reg_addr = instruction[19:15];
 assign src1_reg_addr = instruction[24:20];
 assign dest_reg_addr = instruction[11:7]; 
 
 // Pipe forwarding
 always_comb begin
 
    // ALU IN0
    if ( src0_reg_addr_dline[1] != 0 )
        if ( ( src0_reg_addr_dline[1] ==  dest_reg_addr_dline[3] ) & opcode_dline[1] != 3 )
           alu_in0_pipe_fwd <= alu_out;
        else if ( ( src0_reg_addr_dline[1] ==  dest_reg_addr_dline[5] ) & opcode_dline[3] != 3 )
           alu_in0_pipe_fwd <= alu_out_dline[1];
        else if ( ( src0_reg_addr_dline[1] ==  dest_reg_addr_dline[5] ) & opcode_dline[3] == 3 )
           alu_in0_pipe_fwd <= data_mem_rd_data;
        else
            alu_in0_pipe_fwd <= src0_reg_out;
    else 
	   alu_in0_pipe_fwd <= src0_reg_out;
	       
    
    // ALU IN1
    if ( src1_reg_addr_dline[1] != 0 )
        if ( ( src1_reg_addr_dline[1] ==  dest_reg_addr_dline[3] ) & opcode_dline[1] != 3 )
            alu_in1_pipe_fwd <= alu_out;
        else if ( ( src1_reg_addr_dline[1] ==  dest_reg_addr_dline[5] ) & opcode_dline[3] != 3 )
           alu_in1_pipe_fwd <= alu_out_dline[1];
        else if ( ( src1_reg_addr_dline[1] ==  dest_reg_addr_dline[5] ) & opcode_dline[3] == 3 )
           alu_in1_pipe_fwd <= data_mem_rd_data;
        else
           alu_in1_pipe_fwd <= src1_reg_out; 
    else 
	   alu_in1_pipe_fwd <= src1_reg_out;
 end

 // ALU Inputs
 assign alu_in0 = ( alu_in0_sel ) ? pc_dline[3] : alu_in0_pipe_fwd;
 assign alu_in1 = ( alu_in1_sel ) ? imm : alu_in1_pipe_fwd;
 
 // Data memory inputs
 assign data_mem_wr_data_en = data_mem_wr_en_dline[1];
 assign data_mem_addr       = alu_out[`ALEN-1:0];
 assign data_mem_wr_data    = src1_reg_out_dline[1];
 
 // Sign extention 
 always_comb
    if ( data_mem_rd_sign_ext_mode_dline[1] == 1 )
        data_mem_rd_data_sign_ext  <= { {24{data_mem_rd_data[7]}}, data_mem_rd_data[7:0] };
    else if  ( data_mem_rd_sign_ext_mode_dline[1] == 2 )
        data_mem_rd_data_sign_ext  <= { {16{data_mem_rd_data[15]}}, data_mem_rd_data[15:0] };
    else if ( data_mem_rd_sign_ext_mode_dline[1] == 3 )
        data_mem_rd_data_sign_ext  <= { 24'h000000, data_mem_rd_data[7:0] };
    else if  ( data_mem_rd_sign_ext_mode_dline[1] == 4 )
        data_mem_rd_data_sign_ext  <= { 16'h0000, data_mem_rd_data[15:0] };
    else 
        data_mem_rd_data_sign_ext  <= data_mem_rd_data;
        
 // Dest reg WR data selection
 always_comb
    if ( dest_reg_wr_data_sel_dline[3] == 2'b0 )
        dest_reg_in <= data_mem_rd_data_sign_ext;
    else if ( dest_reg_wr_data_sel_dline[3] == 2'b1 ) 
        dest_reg_in <= alu_out_dline[1];
    else if ( dest_reg_wr_data_sel_dline[3] == 2'b10 )  
        dest_reg_in <= imm_dline[3];
    else 
        dest_reg_in <= pc_dline[5];
        
 // Branch and pipeline hazard 
 always_comb begin
    //instruction_rd[19:15] corresponds to src0_reg addr, and instruction_rd[24:20] corresponds to src1_reg addr
    if ( opcode == 3 & dest_reg_addr_dline[1] != 0 & ( dest_reg_addr_dline[1] == instruction_rd[19:15] | dest_reg_addr_dline[1] == instruction_rd[24:20] ) )
        stall <= 1;
    else
        stall <= 0;
 end
 
 assign interrupt_raised = |interrupt_in;
 assign branch_stall = ( branch | branch_dline[1] );
 
 // Delay Lines
 delay_line #(   32, 4) u_delay_line_imm        ( .in(imm), .out(imm_dline), .* );
 delay_line #(`XLEN, 4) u_delay_line_alu_out    ( .in(alu_out), .out(alu_out_dline), .* );
 delay_line #(`ALEN, 8) u_delay_line_pc         ( .in(pc), .out(pc_dline), .* );
 delay_line #(    2, 4) u_delay_line_dest_in_sel( .in(dest_reg_wr_data_sel), .out(dest_reg_wr_data_sel_dline), .* );
 delay_line #(    3, 2) u_delay_line_sign_ext   ( .in(data_mem_rd_sign_ext_mode), .out(data_mem_rd_sign_ext_mode_dline), .* );
 delay_line #(    1, 2) u_delay_line_mem_wr_en  ( .in(data_mem_wr_en & ~branch_stall), .out(data_mem_wr_en_dline), .* );
 delay_line #(    3, 2) u_delay_line_branch_mode( .in(branch_mode), .out(branch_mode_dline), .* );
 delay_line #(    7, 6) u_delay_line_opcode     ( .in(opcode), .out(opcode_dline), .* ); 
 delay_line #(`XLEN, 2) u_delay_line_src1_data  ( .in(src1_reg_out), .out(src1_reg_out_dline), .* );
 delay_line #(    5, 2) u_delay_line_src0_addr  ( .in(src0_reg_addr), .out(src0_reg_addr_dline), .* ); 
 delay_line #(    5, 2) u_delay_line_src1_addr  ( .in(src1_reg_addr), .out(src1_reg_addr_dline), .* );
 delay_line #(    5, 6) u_delay_line_dest_addr  ( .in(dest_reg_addr), .out(dest_reg_addr_dline), .* );
 delay_line #(   32, 2) u_delay_line_inst_rd    ( .in(instruction_rd), .out(instruction_rd_dline), .* );
 delay_line #(    1, 4) u_delay_line_dest_wr_en ( .in(dest_reg_wr_en & ~branch_stall), .out(dest_reg_wr_en_dline), .* );
 delay_line #(    1, 2) u_delay_line_branch     ( .in(branch | interrupt_raised), .out(branch_dline), .* );
 
 // Interrupt Table (0 cycles latency)
 interrupt_table u_interrupt_table (
    .*
 );
 
 // ALU
 alu u_alu(     
     .alu_op(alu_op),
     
     .in0(alu_in0),
     .in1(alu_in1),
     
     .out(alu_out),    
     .flag(alu_flag),
     
     .*
 );

 // Memory (two cycles latency)
 memory u_memory(  
        .prog_sync_rst(uart_prog_mem_sync_rst),    
        .prog_mem_rd_data(instruction_rd), 
        .*   
 );
 
 // Registers (two cycles latency)
 registers u_registers(
     .src0_addr(src0_reg_addr),
     .src1_addr(src1_reg_addr),
     .dest_addr(dest_reg_addr_dline[5]),
     .dest_wr_en(dest_reg_wr_en_dline[3]),
     .dest_data(dest_reg_in),
     .src0_data(src0_reg_out),
     .src1_data(src1_reg_out),
     .*
 );
     
 // Immediate Generation (two cycles latency) // Immediate generation (two clocks latency)
 imm_gen u_imm_gen(
     .inst(instruction),
     .imm(imm),
     .*
 );
     
 // Control Unit (two clock latency)
 control_unit u_control_unit(
    .*
 ); 
    
 // Branch logic (0 clock latency)
 branch_control u_branch_control(
    .branch_mode(branch_mode_dline[1]),
    .flag(alu_flag),
    .branch(branch) 
 );
    
 // UART
 uart_bridge u_uart_bridge(
     .prog_mem_sync_rst(uart_prog_mem_sync_rst),
     .prog_mem_wr_en(uart_prog_mem_wr_en),
     .prog_mem_addr(uart_prog_mem_wr_addr),
     .prog_mem_wr_data(uart_prog_mem_wr_data),
     .*
 );
 
endmodule
