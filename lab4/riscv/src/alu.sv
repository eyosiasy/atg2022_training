`timescale 1ns / 1ps
`include "defines_inc.sv"

module alu(
    input                   clk,
    input                   async_rst_n,
    input                   sync_rst,
    
    input   [3:0]           alu_op,
    
    input   [`XLEN-1:0]     in0,
    input   [`XLEN-1:0]     in1,
    
    output  [`XLEN-1:0]     out,    
    output  [7:0]           flag
    );

// local copy of output ports
logic [`XLEN:0] out_local, out_local_d;
logic [7:0]     flag_local;
logic [`XLEN-1:0] in0_d,in1_d;

///////////////////////////////////////////
// alu_op key
//
// output zero      0
// left shift       1
// right shift log  2
// right shift arit 3
// add              4
// subtract         5
// xor              6
// or               7
// and              8
// compare signed   9
// compare unsigned A
//
/////////////////////////////////////////////

/////////////////////////////////////////////
//  Flag construction
//
// b0: alu_out == 0 --> 1 else --> 0
// b1: in0 >= in1   --> 1 else --> 0 
// b2: in1 == in1   --> 1 else --> 0 
// b3: alu_out >= 0 --> 1 else --> 0 
// b4: carry == 1   --> 1 else --> 0 
// b5: reserved, set to 0
// b6: reserved, set to 0
// b7: reserved, set to 0
/////////////////////////////////////////////


// At zero time, the output of the arithmetic is calculated
always_ff @ ( posedge clk ) 
    if ( ~async_rst_n ) begin
        out_local <= 0;
        in0_d <= 0;
        in1_d <= 0;
    end
    else if ( sync_rst ) begin
        out_local <= 0;
        in0_d <= 0;
        in1_d <= 0;
    end
    else begin
        in0_d <= in0;
        in1_d <= in1;
        
        case ( alu_op )

        0: begin 
        
            out_local <= 0;
        
        end
                
        1: begin // left shift
        
            out_local <= ( in0 << in1[4:0] );
        
        end
        
        2: begin // right shift
        
            out_local <= in0 >> in1[4:0];
        
        end
        
        3: begin // right shift arithmetic
        
            out_local[`XLEN-1:0] <= signed'(in0) >>> in1[4:0];
            out_local[`XLEN] <= 0;
            
        end

        4: begin // add
        
            out_local <= signed'(in0) + signed'(in1);
        
        end
        
        5: begin // subtract
        
            out_local <= signed'(in0) - signed'(in1);
        
        end
 
        6: begin // xor
        
            out_local <= in0 ^ in1;
        
        end       

        7: begin // or
        
            out_local <= in0 | in1;
        
        end 
        
        8: begin // and
        
            out_local <= in0 & in1;
        
        end 
        
        9: begin // set if less than signed
        
            out_local <= ( signed'(in0) < signed'(in1) ) ? 1 : 0;
        
        end 
        
        10: begin // set if less than unsigned
        
            out_local <= ( in0 < in1 ) ? 1 : 0;
        
        end 
        
        default:
            out_local <= 0;
                
        endcase
    
    
    end

// In the next clock cycle, the flag out is calculated
always_ff @ ( posedge clk )
    if ( ~async_rst_n ) begin
       flag_local <= 0;
       out_local_d <= 0;
    end
    else if ( sync_rst ) begin
       flag_local <= 0;
       out_local_d <= 0;
    end
    else begin
        flag_local[0] <= ( out_local[`XLEN-1:0] == 0        ) ? 1 : 0;
        flag_local[1] <= ( in0_d >= in1_d                   ) ? 1 : 0;
        flag_local[2] <= ( in0_d == in1_d                   ) ? 1 : 0;
        flag_local[3] <= ( out_local[`XLEN-1]               ) ? 0 : 1;
        flag_local[4] <=   out_local[`XLEN];
        flag_local[5] <= ( signed'(in0_d) >= signed'(in1_d) ) ? 1 : 0;
        flag_local[6] <= 0;
        flag_local[7] <= 0;
        
        out_local_d <= out_local;
    end
    
    
// connect local signals to output ports
assign out = out_local_d[`XLEN-1:0];
assign flag = flag_local;

endmodule

