`timescale 1ns / 1ps

`include "defines_inc.sv"

module memory(
    input                   clk,
    input                   async_rst_n,
    input                   sync_rst,
    input                   prog_sync_rst,
    
    // External IO ports
    input                   port_in,
    output                  port_out,
    
    // Program memory interface
    input                   prog_mem_wr_en,
    input   [`ALEN-1:0]     prog_mem_addr,
    input   [31:0]          prog_mem_wr_data,
    output  [31:0]          prog_mem_rd_data,
    
    // Data memory interface
    input                   data_mem_wr_data_en,
    input   [`ALEN-1:0]     data_mem_addr,
    input   [31:0]          data_mem_wr_data,
    output  [31:0]          data_mem_rd_data
    );

logic   [31:0]      port_in_dline[2];
logic   [`ALEN-1:0] data_mem_addr_dline[2];
logic   [31:0]      port_out_reg;
logic   [31:0]      port_out_reg_d;
 
logic   [31:0]      data_mem_rd_data_not_io; 
logic               data_mem_wr_en_not_io;

logic   [15:0]      pmem_addr_in;
logic   [12:0]      dmem_addr_in;

assign pmem_addr_in = prog_mem_addr[17:2] - `PMEM_START_32BIT_WORD_ADDR;
assign dmem_addr_in = data_mem_addr[14:2] - `DMEM_START_32BIT_WORD_ADDR;

pmem u_pmem (
  .clka(clk),    // input wire clka
  .rsta(prog_sync_rst),    // input wire rsta
  .wea({4{prog_mem_wr_en}}),      // input wire [3 : 0] wea
  .addra(pmem_addr_in),  // input wire [15 : 0] addra ---> Input to IP is 32 bit word address
  .dina(prog_mem_wr_data),    // input wire [31 : 0] dina
  .douta(prog_mem_rd_data)  // output wire [31 : 0] douta
);

dmem u_dmem (
  .clka(clk),    // input wire clka
  .rsta(sync_rst),    // input wire rsta
  .wea({4{data_mem_wr_en_not_io}}),      // input wire [3 : 0] wea
  .addra(dmem_addr_in),  // input wire [12 : 0] addra ---> Input to IP is 32 bit word address
  .dina(data_mem_wr_data),    // input wire [31 : 0] dina
  .douta(data_mem_rd_data_not_io)  // output wire [31 : 0] douta
 );
 

 // Delay Lines
 delay_line #(`ALEN, 2) u_delay_dmem_addr   ( .in(data_mem_addr), .out(data_mem_addr_dline), .* );
 delay_line #(32, 2)    u_delay_port_in     ( .in(port_in), .out(port_in_dline), .* );   
   
 // Data Mem Read out
 assign data_mem_rd_data = ( data_mem_addr_dline[1] == `EXT_PORT_IN_ADDR ) ?  port_in_dline[1] : data_mem_rd_data_not_io;
 
 // Data Mem Write control
 assign data_mem_wr_en_not_io = data_mem_wr_data_en  & ( data_mem_addr <= `DMEM_MAX_ADDR & data_mem_addr >= `DMEM_MIN_ADDR  );
 
 always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
        port_out_reg <= 0;
        port_out_reg_d <= 0;   
    end
    else if ( sync_rst ) begin
        port_out_reg <= 0;
        port_out_reg_d <= 0;       
    end
    else begin
        if ( data_mem_addr == `EXT_PORT_OUT_ADDR & data_mem_wr_data_en )
            port_out_reg <= data_mem_wr_data;
       
        port_out_reg_d <= port_out_reg;       
    end
 
 assign port_out = port_out_reg_d;
 
endmodule

