`timescale 1ns / 1ps

module delay_line #(parameter W = 16, parameter D = 2) (
        input           clk,
        input           async_rst_n,
        input           sync_rst,
        
        input   [W-1:0] in,
        output  [W-1:0] out[D]
    );
    
 logic [W-1:0] in_d[D];

 // First delay
 always_ff @ ( posedge clk or negedge async_rst_n ) 
    if ( ~async_rst_n )
        in_d[0] <= 0;
    else if ( sync_rst )
        in_d[0] <= 0;
    else
        in_d[0] <= in;
 
 // Subsequent delays
 genvar ii;
 generate
 for ( ii = 1; ii < D; ii++ ) begin
     always_ff @ ( posedge clk or negedge async_rst_n ) 
        if ( ~async_rst_n )
            in_d[ii] <= 0;
        else if ( sync_rst )
            in_d[ii] <= 0;
        else
            in_d[ii] <= in_d[ii-1];
 end
 endgenerate
 
 assign out = in_d;

endmodule
