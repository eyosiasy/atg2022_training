#########################################################################################################
# This script uses instruction_list.csv to generate soco_control_unit_gen.sv
#########################################################################################################

#########################################################################################################
# How is instruction_list.csv populated?
# It is populated using the opcodes given in https://github.com/riscv/riscv-opcodes/blob/master/opcodes
#########################################################################################################

import csv

# function takes the values to be assigned in a dictionary and assigns them to each port
# if zero flat is 1, all ports will be assigned to zero
def output_signal_gen(inst_dict, zero):
    str = '';
    
    if zero == True:
        inst_dict =  { x:'0' for x in inst_dict }
        
    str += '\t\t branch_mode_local <= 4\'h' + inst_dict['branch_mode'] + ';\n'
    str += '\t\t alu_in0_sel_local <= 1\'h' + inst_dict['alu_in0_sel'] + ';\n'
    str += '\t\t alu_in1_sel_local <= 1\'h' + inst_dict['alu_in1_sel'] + ';\n'
    str += '\t\t data_mem_wr_en_local <= 2\'h' + inst_dict['data_mem_wr_en'] + ';\n'
    str += '\t\t data_mem_rd_en_local <= 2\'h' + inst_dict['data_mem_rd_en'] + ';\n'
    str += '\t\t dest_reg_wr_en_local <= 1\'h' + inst_dict['dest_reg_wr_en'] + ';\n'
    str += '\t\t dest_reg_wr_data_sel_local <= 2\'h' + inst_dict['dest_reg_wr_data_sel'] + ';\n'
    str += '\t\t data_mem_rd_sign_ext_mode_local <= 3\'h' + inst_dict['data_mem_rd_sign_ext_mode'] + ';\n'
    str += '\t\t alu_op_local <= 4\'h' + inst_dict['alu_op'] + ';\n'
    
    return str
    
# function generate the condition line of else if statment for each instruction
def condition_line(inst_dict):
    str = '\n\telse if ('
    
    if ( inst_dict['frmt'] == 'R' ):
        str += ' opcode_d == 7\'h' + inst_dict['opcode'] + ' && func3_d == 3\'h' + inst_dict['func3'] + ' && func7_d == 7\'h' + inst_dict['func7'] + ' )'
    elif ( inst_dict['frmt'] == 'I' or inst_dict['frmt'] == 'S' or inst_dict['frmt'] == 'SB'):
        str += ' opcode_d == 7\'h' + inst_dict['opcode'] + ' && func3_d == 3\'h' + inst_dict['func3'] + ' )'
    else: # 'U' or 'UJ'
        str += ' opcode_d == 7\'h' + inst_dict['opcode'] + ' )'
        
    str += ' begin // ' + inst_dict['instruction'] + '\n'
    
    return str;
    
# Read CSV into a list of dictionaries
csvfile = open('instruction_list.csv')
dict_list = csv.DictReader(csvfile)
    

# Header
str = '''
/////////////////////////////////////////////////////////////////////////
//  This file is autogenerated using soco_control_unit_gen.py
//  based on the data in instruction_list.csv.
//
//  !!!!!! DO NOT UPDATE MANUALLY !!!!!!
//   if update is needed, please make update in instruction_list.csv
//   and/or control_unit_gen.py
///////////////////////////////////////////////////////////////////////


'''
    
# Construct always block
str += '''
always_ff @ ( posedge clk or negedge async_rst_n )
    if ( ~async_rst_n ) begin
'''
  
for row in dict_list:
    str += output_signal_gen( row, True )
    str += '''
    end 
    '''
    break
    
str += '''
    else if ( sync_rst ) begin
'''
  
for row in dict_list:
    str += output_signal_gen( row, True )
    str += '''
    end 
    '''
    break

csvfile.seek(0)
next(dict_list)

for dict in dict_list:
    str += condition_line(dict)
    str += output_signal_gen(dict,False)
    str += '\tend \n\n'
    
str += '\telse begin\n\n'

str += output_signal_gen( row, True )

str += '''
    end 
'''

# write to file
with open( '..\control_unit_inc.sv', 'w' ) as file:
    file.write( str )
    