`timescale 1ns / 1ps

`define ONE_CLK_CYCLE 50
`define ONE_SERIAL_BIT_CYCLE 8681
`define COMPARE( checker_name, exp, actual ) if ( exp !== actual ) begin $display("***** Mismatch @ checker = %s, exp = x%h, actual = x%h", checker_name, exp, actual ); $finish; end

`define RESET_CORE_DLY_COUNT

    
// Delay simulation by D time units
task delay(input int D);
    #D;
endtask

// ----------------------------------------------------------------------
// LUI
// ----------------------------------------------------------------------
task program_lui( input logic [`ALEN-1:0] instruction_addr, input logic [19:0] imm, input logic [4:0] rd );
    
    logic [31:0] instruction;
    instruction = {imm,rd,5'h0D,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lui(input integer seed );
    
    logic [19:0] imm;
    logic [4:0] rd;
    logic [31:0] instruction_addr;
     
    $srandom(seed);
         
    imm = $urandom_range(20'hfffff,0);
    rd  = $urandom_range(31,1);
    //instruction_addr = $urandom_range(18'h3ffff,0);
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr, imm, rd);
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+5*2+(1-1)*2) );
    
    `COMPARE( "CHECK_LUI", {imm, 12'b0}, dut.u_registers.regs[rd] );

endtask


// ----------------------------------------------------------------------
// AUIPC
// ----------------------------------------------------------------------
task program_auipc( input logic [`ALEN-1:0] instruction_addr, input logic [19:0] imm, input logic [4:0] rd );
    
    logic [31:0] instruction;
    instruction = {imm,rd,5'h05,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_auipc(input integer seed );
    
    logic [19:0] imm;
    logic [4:0] rd;
    logic [31:0] instruction_addr;
     
    $srandom(seed);
         
    imm = $urandom_range(20'hfffff,0);
    rd  = $urandom_range(31,1);
    //instruction_addr = $urandom_range(18'h3ffff,0);
    instruction_addr = 0; 
    
    reset_program_mem();
    program_auipc(instruction_addr, imm, rd);
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+5*2+(1-1)*2));//1 -->reset_core latency, 5 --> # of piple line stages (2 cycle per stage), 1 --> # of instructions
    
    `COMPARE( "CHECK_AUIPC", instruction_addr + {imm, 12'b0}, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// ADD
// ----------------------------------------------------------------------
task program_add( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h0,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_add(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = {imm1, 12'b0} + {imm2, 12'b0};
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_add(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+5*2+(5-1)*2) ); //1 --> reset_core latency, first 5 --> # of piple line stages (2 cycle per stage), second 5 --> # of instructions
    
    `COMPARE( "CHECK_ADD", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// ADDI
// ----------------------------------------------------------------------
task program_addi( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h0,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_addi(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) + signed'(imm2);
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  0,  0, 0 );
    program_addi(instruction_addr+8,  0,  0, 0 );
    program_addi(instruction_addr+12,  rd,   0, 0 );
    program_addi(instruction_addr+16, rd, rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * ((5*2)+8) );
    
    `COMPARE( "CHECK_ADDI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SUB
// ----------------------------------------------------------------------
task program_sub( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h20,rs2,rs1,3'h0,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_sub(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = {imm1, 12'b0} - {imm2, 12'b0};
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_sub(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_SUB", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// XOR
// ----------------------------------------------------------------------
task program_xor( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h4,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_xor(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = {imm1, 12'b0} ^ {imm2, 12'b0};
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_xor(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_XOR", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// XORI
// ----------------------------------------------------------------------
task program_xori( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h4,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_xori(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) ^ signed'(imm2);
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rd,  0, 0 );
    program_addi(instruction_addr+8,  rd,   0, 0 );
    program_xori(instruction_addr+12, rd, rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_XORI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// OR
// ----------------------------------------------------------------------
task program_or( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h6,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_or(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = {imm1, 12'b0} | {imm2, 12'b0};
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_or(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_OR", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// ORI
// ----------------------------------------------------------------------
task program_ori( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h6,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_ori(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) | signed'(imm2);
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rd,  0, 0 );
    program_addi(instruction_addr+8, rd,   0, 0 );
    program_ori(instruction_addr+12, rd, rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_ORI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// AND
// ----------------------------------------------------------------------
task program_and( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h7,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_and(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = {imm1, 12'b0} & {imm2, 12'b0};
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_and(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_AND", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// ANDI
// ----------------------------------------------------------------------
task program_andi( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h7,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_andi(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) & signed'(imm2);
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rd,  0, 0 );
    program_addi(instruction_addr+8, rd,   0, 0 );
    program_andi(instruction_addr+12, rd, rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_ANDI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SLL
// ----------------------------------------------------------------------
task program_sll( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h1,rd,5'h0C,2'h3};

    program_if(instruction_addr, instruction);

endtask


task check_sll(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1,rs2;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) << imm2[4:0];
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rs2,  0, imm2 );
    program_lui(instruction_addr+8,  0, 0 );
    program_lui(instruction_addr+12, 0, 0 );
    program_sll (instruction_addr+16, rd, rs1, rs2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+5*2+(5-1)*2) );
    
    `COMPARE( "CHECK_SLL", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SLLI
// ----------------------------------------------------------------------
task program_slli( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] shamt );
    
    logic [31:0] instruction;
    instruction = {7'h0,shamt,rs1,3'h1,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_slli(input integer seed );
    
    
    logic [11:0] imm1;
    logic [4:0] shamt;
    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);

    imm1 = $urandom_range(12'hfff,0);
    shamt = $urandom_range(5'h1f,0);
    
    exp_add_out = signed'(imm1) << shamt;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
    
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,   0, imm1 );
    program_addi(instruction_addr+4,  rs_dummy,  rs_dummy, 0 );
    program_addi(instruction_addr+8,  rs_dummy,  rs_dummy, 0 );
    program_slli(instruction_addr+12, rd,  rs1, shamt );
    
    program_if(instruction_addr+16, 0); // Not sure why this is necessary, but it is to pass

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_SLLI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SRL
// ----------------------------------------------------------------------
task program_srl( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h5,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_srl(input integer seed );
    
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1,rs2;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
          
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) >> imm2[4:0];
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rs2,  0, imm2 );
    program_lui(instruction_addr+8,  0, 0 );
    program_lui(instruction_addr+12, 0, 0 );
    program_srl (instruction_addr+16, rd, rs1, rs2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_SRL", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SRLI
// ----------------------------------------------------------------------
task program_srli( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] shamt );
    
    logic [31:0] instruction;
    instruction = {7'h0,shamt,rs1,3'h5,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_srli(input integer seed );
    
    
    logic [11:0] imm1;
    logic [4:0] shamt;
    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    shamt = $urandom_range(5'h1f,0);
    
    exp_add_out = signed'(imm1) >> shamt;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
 
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
       
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rs_dummy,  rs_dummy, 0 );
    program_addi(instruction_addr+8,  rs_dummy,  rs_dummy, 0 );
    program_srli(instruction_addr+12, rd, rs1, shamt );
    program_if(instruction_addr+16, 0); // Not sure why this is necessary, but it is to pass
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_SRLI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SRA
// ----------------------------------------------------------------------
task program_sra( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h20,rs2,rs1,3'h5,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_sra(input integer seed );
     
    logic [11:0] imm1,imm2;
    logic [4:0]  rd,rs1,rs2;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
          
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) >>> imm2[4:0];
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rs2,  0, imm2 );
    program_lui(instruction_addr+8,  0, 0 );
    program_lui(instruction_addr+12, 0, 0 );
    program_sra (instruction_addr+16, rd, rs1, rs2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_SRA", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SRAI
// ----------------------------------------------------------------------
task program_srai( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] shamt );
    
    logic [31:0] instruction;
    instruction = {7'h10,shamt,rs1,3'h5,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_srai(input integer seed);

    
    logic [11:0] imm1;
    logic [4:0] shamt;
    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
      
    imm1 = $urandom_range(12'hfff,0);
    shamt = $urandom_range(5'h1f,0);
    
    exp_add_out = signed'(imm1) >>> shamt;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
        
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rs_dummy,  rs_dummy, 0 );
    program_addi(instruction_addr+8,  rs_dummy,  rs_dummy, 0 );
    program_srai(instruction_addr+12, rd, rs1, shamt );
    program_if(instruction_addr+16, 0); // Not sure why this is necessary, but it is to pass
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_SRAI", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// LB
// ----------------------------------------------------------------------
task program_lb( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h0,rd,5'h0,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lb(input integer seed);

    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] base, offset, data_word_addr;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [7:0] exp_add_out_pre;
    
    $srandom(seed);
      
    base = `DMEM_MIN_ADDR ;
    offset = $urandom_range(31,0)<<2;
    data_word_addr = ( signed'(base) + signed'(offset) ) >> 2;
    
    
    exp_add_out_pre = data_mem_mdl[data_word_addr[4:0]][7:0];
    exp_add_out = { {24{exp_add_out_pre[7]}} , exp_add_out_pre };
    base = base + (`DMEM_START_32BIT_WORD_ADDR << 2 );
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr,    (base[19:0]>>12), rs1  );
    program_lui(instruction_addr+4,  0,  0 );
    program_lui(instruction_addr+8,  0,  0 );
    program_lb (instruction_addr+12, rd, rs1, offset[11:0] );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_LB", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// LH
// ----------------------------------------------------------------------
task program_lh( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h1,rd,5'h0,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lh(input integer seed);

    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] base, offset, data_word_addr;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [15:0] exp_add_out_pre;
    
    $srandom(seed);
      
    base = `DMEM_MIN_ADDR ;
    offset = $urandom_range(31,0)<<2;
    data_word_addr = ( signed'(base) + signed'(offset) ) >> 2;
    
    exp_add_out_pre = data_mem_mdl[data_word_addr[4:0]][15:0]; // little endian
    exp_add_out = { {16{exp_add_out_pre[15]}} , exp_add_out_pre };
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr,   (base[19:0]>>12), rs1  );
    program_lui(instruction_addr+4,  0,  0 );
    program_lui(instruction_addr+8,  0,  0 );
    program_lh(instruction_addr+12, rd, rs1, offset[11:0] );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_LH", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// LW
// ----------------------------------------------------------------------
task program_lw( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h2,rd,5'h0,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lw(input integer seed);

    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] base, offset, data_word_addr;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
      
    base = `DMEM_MIN_ADDR ;
    offset = $urandom_range(31,0)<<2;
    data_word_addr = ( signed'(base) + signed'(offset) ) >> 2;
    
    exp_add_out = data_mem_mdl[data_word_addr[4:0]]; // little endian
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr,    (base[19:0]>>12), rs1  );
    program_lui(instruction_addr+4,  0,  0 );
    program_lui(instruction_addr+8,  0,  0 );
    program_lw (instruction_addr+12, rd, rs1, offset[11:0] );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_LW", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// LBU
// ----------------------------------------------------------------------
task program_lbu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h4,rd,5'h0,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lbu(input integer seed);

    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] base, offset, data_word_addr;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [7:0] exp_add_out_pre;
    
    $srandom(seed);
      
    base = `DMEM_MIN_ADDR ;
    offset = $urandom_range(31,0)<<2;
    data_word_addr = ( signed'(base) + signed'(offset) ) >> 2;
    
    exp_add_out_pre = data_mem_mdl[data_word_addr[4:0]][7:0];
    exp_add_out = { {24{0}} , exp_add_out_pre };
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr,    (base[19:0]>>12), rs1  );
    program_lui(instruction_addr+4,  0,  0 );
    program_lui(instruction_addr+8,  0,  0 );
    program_lbu(instruction_addr+12, rd, rs1, offset[11:0] );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_LBU", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// LHU
// ----------------------------------------------------------------------
task program_lhu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h5,rd,5'h0,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_lhu(input integer seed);

    logic [4:0] rd,rs1,rs_dummy;
    logic [31:0] base, offset, data_word_addr;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [15:0] exp_add_out_pre;
    
    $srandom(seed);
      
    base = `DMEM_MIN_ADDR ;
    offset = $urandom_range(31,0)<<2;
    data_word_addr = ( signed'(base) + signed'(offset) ) >> 2;
    
    exp_add_out_pre = data_mem_mdl[data_word_addr[4:0]][15:0]; // little endian
    exp_add_out = { {16{0}} , exp_add_out_pre };
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr,    (base[19:0]>>12), rs1  );
    program_lui(instruction_addr+4,  0,  0 );
    program_lui(instruction_addr+8,  0,  0 );
    program_lhu(instruction_addr+12, rd, rs1, offset[11:0] );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+4*2) );
    
    `COMPARE( "CHECK_LHU", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SB
// ----------------------------------------------------------------------
task program_sb( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[11:5],rs2,rs1,3'h0,imm[4:0],5'h8,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_sb(input integer seed);

    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] base, offset, str_addr, data_word_addr, ld_base, ld_offset, ld_word_addr_offset;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [15:0] exp_add_out_pre;
    
    $srandom(seed);
      
    do begin  
        ld_word_addr_offset = $urandom_range(31,0);
    end while ( ld_word_addr_offset == 0 );
    
    
    ld_offset = ld_word_addr_offset << 2;
    ld_base = `DMEM_MIN_ADDR;
    
    str_addr = $urandom_range(73664,65536) << 2 ;
    base = {str_addr[31:12],12'b0};
    offset = str_addr[11:0];
    
    exp_add_out_pre = data_mem_mdl[ld_word_addr_offset];
    exp_add_out = { {24{exp_add_out_pre[7]}} , exp_add_out_pre[7:0] };
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui (instruction_addr,    (ld_base>>12), rs1  );
    program_lui (instruction_addr+4,  0,   0 );
    program_lui (instruction_addr+8,  0,   0 );
    program_lw  (instruction_addr+12, rs2, rs1, ld_offset[11:0] );
    program_lui (instruction_addr+16, (base>>12), rs1);
    program_lui (instruction_addr+20, 0,   0 );
    program_lui (instruction_addr+24, 0,   0 );
    program_sb  (instruction_addr+28, rs2, rs1, offset[11:0] );
    program_lui (instruction_addr+32, 0,   0 );
    program_lui (instruction_addr+36, 0,   0 );
    program_lw  (instruction_addr+40, rd, rs1, offset[11:0] );    
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+11*2) );
    
    `COMPARE( "CHECK_SB", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SH
// ----------------------------------------------------------------------
task program_sh( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[11:5],rs2,rs1,3'h0,imm[4:0],5'h8,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_sh(input integer seed);

    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] base, offset, str_addr, data_word_addr, ld_base, ld_offset, ld_word_addr_offset;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [15:0] exp_add_out_pre;
    
    $srandom(seed);
      
    do begin  
        ld_word_addr_offset = $urandom_range(31,0);
    end while ( ld_word_addr_offset == 0 );
    
    
    ld_offset = ld_word_addr_offset << 2;
    ld_base = `DMEM_MIN_ADDR;
    
    str_addr = $urandom_range(73664,65536) << 2 ;
    base = {str_addr[31:12],12'b0};
    offset = str_addr[11:0];
    
    exp_add_out_pre = data_mem_mdl[ld_word_addr_offset];
    exp_add_out = { {24{exp_add_out_pre[15]}} , exp_add_out_pre[15:0] };
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui (instruction_addr,    (ld_base>>12), rs1  );
    program_lui (instruction_addr+4,  0,   0 );
    program_lui (instruction_addr+8,  0,   0 );
    program_lw  (instruction_addr+12, rs2, rs1, ld_offset[11:0] );
    program_lui (instruction_addr+16, (base>>12), rs1);
    program_lui (instruction_addr+20, 0,   0 );
    program_lui (instruction_addr+24, 0,   0 );
    program_sh  (instruction_addr+28, rs2, rs1, offset[11:0] );
    program_lui (instruction_addr+32, 0,   0 );
    program_lui (instruction_addr+36, 0,   0 );
    program_lw  (instruction_addr+40, rd, rs1, offset[11:0] );   
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+11*2) );
    
    `COMPARE( "CHECK_SH", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SW
// ----------------------------------------------------------------------
task program_sw( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[11:5],rs2,rs1,3'h2,imm[4:0],5'h8,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_sw(input integer seed);

    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] base, offset, str_addr, data_word_addr, ld_base, ld_offset, ld_word_addr_offset;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    logic [15:0] exp_add_out_pre;
    
    $srandom(seed);
      
    do begin  
        ld_word_addr_offset = $urandom_range(31,0);
    end while ( ld_word_addr_offset == 0 );
    
    
    ld_offset = ld_word_addr_offset << 2;
    ld_base = `DMEM_MIN_ADDR;
    
    str_addr = $urandom_range(73664,65536) << 2 ;
    base = {str_addr[31:12],12'b0};
    offset = str_addr[11:0];
    
    exp_add_out = data_mem_mdl[ld_word_addr_offset];
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui (instruction_addr,    (ld_base>>12), rs1  );
    program_lui (instruction_addr+4,  0,   0 );
    program_lui (instruction_addr+8,  0,   0 );
    program_lw  (instruction_addr+12, rs2, rs1, ld_offset[11:0] );
    program_lui (instruction_addr+16, (base>>12), rs1);
    program_lui (instruction_addr+20, 0,   0 );
    program_lui (instruction_addr+24, 0,   0 );
    program_sw  (instruction_addr+28, rs2, rs1, offset[11:0] );
    program_lui (instruction_addr+32, 0,   0 );
    program_lui (instruction_addr+36, 0,   0 );
    program_lw  (instruction_addr+40, rd, rs1, offset[11:0] );    
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+11*2) );
    
    `COMPARE( "CHECK_SH", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SLT
// ----------------------------------------------------------------------
task program_slt( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h2,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_slt(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = ( signed'( imm1 ) < signed'( imm2 ) ) ? 1 : 0;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8  , 0, rs_dummy); //NOI
    program_lui(instruction_addr+12 , 0, rs_dummy); //NOI
    program_slt(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_SLT", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SLTI
// ----------------------------------------------------------------------
task program_slti( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h2,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_slti(input integer seed );
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = ( signed'( imm1 ) < signed'( imm2 ) ) ? 1 : 0;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1, 0, imm1 );
    program_lui (instruction_addr+4,  0,   0 );
    program_lui (instruction_addr+8,  0,   0 );
    program_slti(instruction_addr+12, rd,  rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * ((4*2)+8) );
    
    `COMPARE( "CHECK_SLTI", exp_add_out, dut.u_registers.regs[rd] );

endtask


// ----------------------------------------------------------------------
// SLTU
// ----------------------------------------------------------------------
task program_sltu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [4:0] rs2 );
    
    logic [31:0] instruction;
    instruction = {7'h0,rs2,rs1,3'h3,rd,5'h0C,2'h3};
    
    program_if(instruction_addr, instruction);

endtask


task check_sltu(input integer seed );
    
    logic [19:0] imm1, imm2;
    logic [4:0] rd,rs1,rs2, rs_dummy;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(20'hfffff,0);
    imm2 = $urandom_range(20'hfffff,0);
    
    exp_add_out = ( imm1 < imm2 ) ? 1 : 0;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd || rs_dummy == rs1 || rs_dummy == rs2 ); 

    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr    , imm1, rs1);
    program_lui(instruction_addr+4  , imm2, rs2);
    program_lui(instruction_addr+8,   0,   0 );
    program_lui(instruction_addr+12,  0,   0 );
    program_sltu(instruction_addr+16 , rd, rs1, rs2 );
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (8+5*2) );
    
    `COMPARE( "CHECK_SLTU", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// SLTIU
// ----------------------------------------------------------------------
task program_sltiu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h3,rd,5'h04,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_sltiu(input integer seed );
    
    logic [11:0] imm1,imm2;
    logic [4:0] rd,rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = ( imm1 < imm2 ) ? 1 : 0;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );

    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs1,  0, imm1 );
    program_lui (instruction_addr+4,  0,   0 );
    program_lui (instruction_addr+8,  0,   0 );
    program_sltiu(instruction_addr+12, rd, rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * ((4*2)+8) );
    
    `COMPARE( "CHECK_SLTIU", exp_add_out, dut.u_registers.regs[rd] );

endtask


// ----------------------------------------------------------------------
// JAL
// ----------------------------------------------------------------------
task program_jal( input logic [`ALEN-1:0] instruction_addr,input logic [4:0] rd, input logic [20:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[20],imm[10:1],imm[11],imm[19:12],rd,5'h1b,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_jal(input integer seed);

    logic [4:0]  rd1, rd2;
    logic [20:0] offset;
    logic [11:0] imm1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out1, exp_add_out2;
    
    $srandom(seed);
    offset = signed'( $urandom_range(4094,0) ) - signed'(2047) ;
    imm1 = $urandom_range(12'hfff,0);
    
    rd1  = $urandom_range(31,1);
    
    do begin
        rd2  = $urandom_range(31,1);
    end while ( rd2 == rd1 );

    instruction_addr = 0; 
        
    exp_add_out1 = instruction_addr+4;
    exp_add_out2 = signed'(imm1);


    
    reset_program_mem();
    program_jal(instruction_addr,  rd1, offset);
    program_addi(instruction_addr+4,rd2, 0, (imm1+1)  );
    program_lui(instruction_addr+8,  0,   0 );  
    program_addi(instruction_addr+offset,rd2, 0, imm1  );
         
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+(5*2)+(3*2) ) );
    
    `COMPARE( "CHECK_JAL1", exp_add_out1, dut.u_registers.regs[rd1] );
    `COMPARE( "CHECK_JAL2", exp_add_out2, dut.u_registers.regs[rd2] );

endtask

// ----------------------------------------------------------------------
// JALR
// ----------------------------------------------------------------------
task program_jalr( input logic [`ALEN-1:0] instruction_addr,input logic [4:0] rd, input logic [4:0] rs1, input logic [11:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm,rs1,3'h0,rd,5'h19,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_jalr(input integer seed);

    logic [4:0]  rd1,rd2, rs;
    logic [31:0] target_addr;
    logic [11:0] offset;
    logic [11:0] imm1, imm2;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out1, exp_add_out2;
    
    $srandom(seed);
    
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);

    offset = signed'( $urandom_range(4094,0) ) - signed'(2047) ;;
    target_addr = signed'(offset) + signed'(imm1); 
    target_addr[1:0] = 2'b0;
    
    exp_add_out1 = 16;
    exp_add_out2 = signed'(imm2);
    
    rd1  = $urandom_range(31,1);
    
    do begin
        rd2  = $urandom_range(31,1);
    end while ( rd2 == rd1 );
 
     do begin
        rs  = $urandom_range(31,1);
    end while ( rs == rd1 || rs == rd2 );   
         
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rs,   0, imm1 );
    program_lui(instruction_addr+4,    0,   0 );
    program_lui(instruction_addr+8,    0,   0 );  
    program_jalr(instruction_addr+12, rd1, rs, offset);
    program_addi(instruction_addr+16,rd2, 0, (imm2+1)  );
    program_lui(instruction_addr+20,   0,   0 );
    program_lui(instruction_addr+24,   0,   0 );    
    program_addi(target_addr,rd2, 0, imm2  );
         
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+(8*2)+(7*2) ) );
    
    `COMPARE( "CHECK_JALR1", exp_add_out1, dut.u_registers.regs[rd1] );
    `COMPARE( "CHECK_JALR2", exp_add_out2, dut.u_registers.regs[rd2] );

endtask

// ----------------------------------------------------------------------
// BEQ
// ----------------------------------------------------------------------
task program_beq( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h0,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_beq(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hffe,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 2 );
    program_addi(instruction_addr+8,  rs2,  0, 2 );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_beq (instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  ); 
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 3 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_beq (instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BEQ", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// BNEQ
// ----------------------------------------------------------------------
task program_bneq( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h1,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_bneq(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hfff,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 2 );
    program_addi(instruction_addr+8,  rs2,  0, 3 );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_bneq (instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 2 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_bneq (instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BNEQ", exp_add_out, dut.u_registers.regs[rd] );

endtask


// ----------------------------------------------------------------------
// BLT
// ----------------------------------------------------------------------
task program_blt( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h4,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_blt(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hfff,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 12'hfff );
    program_addi(instruction_addr+8,  rs2,  0, 3 );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_blt (instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 12'hfff );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_blt (instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BLT", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// BGE
// ----------------------------------------------------------------------
task program_bge( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h5,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_bge(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hfff,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 12'hfff );
    program_addi(instruction_addr+8,  rs2,  0, 12'hfff );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_bge (instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 3 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_bge (instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BGE", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// BLT
// ----------------------------------------------------------------------
task program_bltu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h6,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_bltu(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hfff,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 3 );
    program_addi(instruction_addr+8,  rs2,  0, 12'hfff );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_bltu(instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 3 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_bltu(instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BLTU", exp_add_out, dut.u_registers.regs[rd] );

endtask

// ----------------------------------------------------------------------
// BGEU
// ----------------------------------------------------------------------
task program_bgeu( input logic [`ALEN-1:0] instruction_addr, input logic [4:0] rs2, input logic [4:0] rs1, input logic [12:0] imm );
    
    logic [31:0] instruction;
    instruction = {imm[12],imm[10:5],rs2,rs1,3'h7,imm[4:1],imm[11],5'h18,2'h3};
    
    program_if(instruction_addr, instruction);

endtask

task check_bgeu(input integer seed);

    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hfff,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 3 );
    program_addi(instruction_addr+8,  rs2,  0, 3 );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_bgeu(instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 4 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_bgeu(instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+28, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+32, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+17*2) );
    
    `COMPARE( "CHECK_BGE", exp_add_out, dut.u_registers.regs[rd] );

endtask

//----------------------------------------------------------
// Data Hazard Tests
//----------------------------------------------------------
task check_data_hazard_with_alu_out_d1(input integer seed );
    
    logic [11:0] imm1;
    logic [4:0]  rd;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) * 2;
    
    rd  = $urandom_range(31,1);
       
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,    rd,  0, imm1 );
    program_addi(instruction_addr+4,  rd, rd, imm1 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1 + (5*2)+ 1*2 ) ); // no stall expected
    
    `COMPARE( "CHECK_DATA_HAZARD_WITH_ALU_OUT_D1", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_data_hazard_with_alu_out_d2(input integer seed );
    
    logic [11:0] imm1, imm2;
    logic [4:0]  rd, rs1;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);
    
    exp_add_out = signed'(imm1) + signed'(imm2);
    
    rd  = $urandom_range(31,1);

    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
       
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr,   rs1,  0, imm1 );
    program_addi(instruction_addr+4,  rd,  0, 0 );
    program_addi(instruction_addr+8,  rd,rs1, imm2 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+(5*2)+2*2) ); // no stall expected
    
    `COMPARE( "CHECK_DATA_HAZARD_WITH_ALU_OUT_D2", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_data_hazard_with_mem_rd_d1(input integer seed );
    
    logic [11:0] imm1;
    logic [4:0]  rd, rs;
    logic [31:0] instruction_addr, ld_word_addr, offset;
    logic [19:0] base;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(31,2);
    ld_word_addr = $urandom_range(31,2);
    
    exp_add_out = signed'(data_mem_mdl[ld_word_addr[4:0]]) + signed'(imm1); 
    base = `DMEM_MIN_ADDR;
    offset = ld_word_addr << 2;
        
    rd  = $urandom_range(31,1);
    rs  = $urandom_range(31,1);   
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr, (base>>12) , rs);
    program_lw(instruction_addr+4,    rd, rs, offset[11:0] );
    program_addi(instruction_addr+8, rd, rd, imm1 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1 + (5*2)+ 2*2 + 2) ); // stall expected
    
    `COMPARE( "CHECK_DATA_HAZARD_WITH_MEM_RD_D1", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_data_hazard_with_mem_rd_d2(input integer seed );
    
    logic [11:0] imm1;
    logic [4:0]  rd, rs;
    logic [31:0] instruction_addr, ld_word_addr, offset;
    logic [19:0] base;
    logic [31:0] exp_add_out;
     
    $srandom(seed);
         
    imm1 = $urandom_range(31,2);
    ld_word_addr = $urandom_range(31,2);
    
    exp_add_out = signed'(data_mem_mdl[ld_word_addr[4:0]]) + signed'(imm1); 
    base = `DMEM_MIN_ADDR;
    offset = ld_word_addr << 2;
        
    rd  = $urandom_range(31,1);
    rs  = $urandom_range(31,1);   
    instruction_addr = 0; 
    
    reset_program_mem();
    program_lui(instruction_addr, (base>>12) , rs);
    program_lw(instruction_addr+4,    rd, rs, offset[11:0] );
    program_lui(instruction_addr+8, 0, 0 );
    program_addi(instruction_addr+12, rd, rd, imm1 );

    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1 + (5*2)+ 3*2 ) ); // no stall expected
    
    `COMPARE( "CHECK_DATA_HAZARD_WITH_MEM_RD_D2", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_control_hazard_1(input integer seed);
// No data hazard
    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hffe,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 2 );
    program_addi(instruction_addr+8,  rs2,  0, 2 );
    program_addi(instruction_addr+12, rs_dummy,  0, 0 );
    program_addi(instruction_addr+16, rs_dummy,  0, 0 );
    program_beq (instruction_addr+20, rs2,  rs1, offset );
    program_addi(instruction_addr+24, rs1,  0, 0 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+28, rs2,  0, 1 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset,rd, 0, imm  );
    program_addi(instruction_addr+20+offset+4,  rs2,  0, 3 );
    program_addi(instruction_addr+20+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+20+offset+12, rs_dummy,  0, 0 );
    program_beq (instruction_addr+20+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+20+offset+20, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+14*2) );
    
    `COMPARE( "CHECK_CONTROL_HAZARD_1", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_control_hazard_2(input integer seed);
// with data hazard
    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hffe,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 2 );
    program_addi(instruction_addr+8,  rs2,  0, 2 );
    program_beq (instruction_addr+12, rs2,  rs1, offset );
    program_addi(instruction_addr+16, rs1,  0, 0 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+20, rs2,  0, 1 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+24, rs_dummy,  0, 0 );
    program_addi(instruction_addr+12+offset,rd, 0, imm  );
    program_addi(instruction_addr+12+offset+4,  rs2,  0, 3 );
    program_addi(instruction_addr+12+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+12+offset+12, rs_dummy,  0, 0 );
    program_beq (instruction_addr+12+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+12+offset+20, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+15*2) );
    
    `COMPARE( "CHECK_CONTROL_HAZARD_2", exp_add_out, dut.u_registers.regs[rd] );

endtask


task check_control_hazard_3(input integer seed);
// with data hazard
    logic [4:0]  rd,rs1,rs2, rs_dummy;
    logic [31:0] offset;
    logic [11:0] imm;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out;
    
    $srandom(seed);
    
    imm = $urandom_range(12'hffe,0);
    
    do begin
        offset = $urandom_range(32'h1fff,0);
    end while (  offset[12] == 1 || offset[0] != 0 );
    
    exp_add_out = signed'(imm) + 1;
    
    rd  = $urandom_range(31,1);
    
    do begin
        rs1  = $urandom_range(31,1);
    end while ( rs1 == rd );
    
    do begin
        rs2  = $urandom_range(31,1);
    end while ( rs2 == rd || rs2 == rs1 );
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rs1 || rs_dummy == rs2 || rs_dummy == rd );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_addi(instruction_addr+4,  rs1,  0, 3 );
    program_addi(instruction_addr+8,  rs2,  0, 2 );
    program_bneq(instruction_addr+12, rs2,  rs1, offset );
    program_addi(instruction_addr+16, rs1,  0, 0 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+20, rs2,  0, 1 ); //overwriting, but will be averted by stall
    program_addi(instruction_addr+32, rs_dummy,  0, 0 );
    program_addi(instruction_addr+12+offset,rd, 0, imm  );
    program_addi(instruction_addr+12+offset+4,  rs2,  0, 4 );
    program_addi(instruction_addr+12+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+12+offset+12, rs_dummy,  0, 0 );
    program_beq (instruction_addr+12+offset+16, rs2,  rs1, offset );
    program_addi(instruction_addr+12+offset+20, rd, 0, (imm+1)  ); 
     
    
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (10+15*2) );
    
    `COMPARE( "CHECK_CONTROL_HAZARD_3", exp_add_out, dut.u_registers.regs[rd] );

endtask

task check_control_hazard_4(input integer seed);

    logic [4:0]  rd1,rd2, rd3, rs_dummy;
    logic [20:0] offset;
    logic [11:0] imm1, imm2;
    logic [31:0] instruction_addr;
    logic [31:0] exp_add_out1, exp_add_out2, exp_add_out3;
    
    $srandom(seed);
    
    imm1 = $urandom_range(12'hfff,0);
    imm2 = $urandom_range(12'hfff,0);

    do begin
        offset = $urandom_range(32'hffff,0);
    end while ( offset[0] != 0 );
    
    exp_add_out1 = signed'(imm1);
    exp_add_out2 = signed'(imm2);
    exp_add_out3 = offset + 4 + 4;
    
    rd1  = $urandom_range(31,1);
    
    do begin
        rd2  = $urandom_range(31,1);
    end while ( rd2 == rd1 );
 
     do begin
        rd3  = $urandom_range(31,1);
    end while ( rd3 == rd1 || rd3 == rd2 );   
    
    do begin
        rs_dummy  = $urandom_range(31,1);
    end while ( rs_dummy == rd1 || rs_dummy == rd2 || rs_dummy == rd3 );
           
    instruction_addr = 0; 
    
    reset_program_mem();
    program_jal(instruction_addr, 0, offset);
    program_addi(instruction_addr+4, rd2,0, imm2  );
    program_addi(instruction_addr+offset,rd1, 0, imm1  );
    program_jal(instruction_addr+offset+4, rd3, 0 - offset );
    program_addi(instruction_addr+offset+8, rs_dummy,  0, 0 );
    program_addi(instruction_addr+offset+12, rs_dummy,  0, 0 );
         
    delay(100);
    reset_core();
    
    delay(`ONE_CLK_CYCLE * (-1+(5*2)+7*2) );
    
    `COMPARE( "CHECK_CONTROL_HAZARD_4_1", exp_add_out1, dut.u_registers.regs[rd1] );
    `COMPARE( "CHECK_CONTROL_HAZARD_4_2", exp_add_out2, dut.u_registers.regs[rd2] );
    `COMPARE( "CHECK_CONTROL_HAZARD_4_3", exp_add_out3, dut.u_registers.regs[rd3] );

endtask


//---------------------------------------------------------------
// Checks each R32I instruction one at a time
//---------------------------------------------------------------
task check_all_instructions(input integer seed);

#`ONE_CLK_CYCLE check_lui(seed );
#`ONE_CLK_CYCLE check_auipc(seed );
//#`ONE_CLK_CYCLE check_add(seed );
#`ONE_CLK_CYCLE check_addi(seed );
//#`ONE_CLK_CYCLE check_sub(seed );
//#`ONE_CLK_CYCLE check_xor(seed );
#`ONE_CLK_CYCLE check_xori(seed );
//#`ONE_CLK_CYCLE check_or(seed );
#`ONE_CLK_CYCLE check_ori(seed );
//#`ONE_CLK_CYCLE check_and(seed );
#`ONE_CLK_CYCLE check_andi(seed );
//#`ONE_CLK_CYCLE check_sll(seed );
//#`ONE_CLK_CYCLE check_slli(seed );
//#`ONE_CLK_CYCLE check_srl(seed );
#`ONE_CLK_CYCLE check_srli(seed );
//#`ONE_CLK_CYCLE check_sra(seed );
//#`ONE_CLK_CYCLE check_srai(seed);
//#`ONE_CLK_CYCLE check_lb(seed);
//#`ONE_CLK_CYCLE check_lh(seed);
//#`ONE_CLK_CYCLE check_lw(seed);
//#`ONE_CLK_CYCLE check_lbu(seed);
//#`ONE_CLK_CYCLE check_lhu(seed);
//#`ONE_CLK_CYCLE check_sb(seed);
//#`ONE_CLK_CYCLE check_sh(seed);
//#`ONE_CLK_CYCLE check_sw(seed);
//#`ONE_CLK_CYCLE check_slt(seed);
//#`ONE_CLK_CYCLE check_slti(seed);
//#`ONE_CLK_CYCLE check_sltu(seed);
//#`ONE_CLK_CYCLE check_sltiu(seed);
//#`ONE_CLK_CYCLE check_jal(seed);
//#`ONE_CLK_CYCLE check_jalr(seed);
//#`ONE_CLK_CYCLE check_beq(seed);
//#`ONE_CLK_CYCLE check_bneq(seed);
//#`ONE_CLK_CYCLE check_blt(seed);
//#`ONE_CLK_CYCLE check_bge(seed);
//#`ONE_CLK_CYCLE check_bltu(seed);
//#`ONE_CLK_CYCLE check_bgeu(seed);

//#`ONE_CLK_CYCLE check_data_hazard_with_alu_out_d2( seed );
//#`ONE_CLK_CYCLE check_data_hazard_with_alu_out_d2( seed );
//#`ONE_CLK_CYCLE check_data_hazard_with_mem_rd_d1( seed );
//#`ONE_CLK_CYCLE check_data_hazard_with_mem_rd_d2( seed );

//#`ONE_CLK_CYCLE check_control_hazard_1( seed );
//#`ONE_CLK_CYCLE check_control_hazard_2( seed );
//#`ONE_CLK_CYCLE check_control_hazard_3( seed );
//#`ONE_CLK_CYCLE check_control_hazard_4( seed );

endtask
