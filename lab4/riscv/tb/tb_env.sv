`timescale 1ns / 1ps
`include "../src/defines_inc.sv"

program tb_env(

    output          clk,
    output          sync_rst,
    output          async_rst_n,
      
    // UAR
    input           uart_tx,  
    output          uart_rx,
    input           loading_program,
    
    output  [31:0]  port_in,
    input   [31:0]  port_out,
    
    output  [7:0]   interrupt_in   
    
    );
    
// simulation seed
integer seed;
integer is_pass;

// local variables
logic                   local_sync_rst;
logic                   local_async_rst_n;
logic                   local_prog_mem_wr_en;
logic   [`ALEN-1:0]     local_prog_mem_addr;
logic   [`XLEN-1:0]     local_prog_mem_wr_data;
logic                   local_uart_rx;
logic                   local_clk; 
logic   [7:0]           local_interrupt_in;   

// data memory model (copy of data_mem.coe loaded to data memory)
logic [31:0] data_mem_mdl[32];
initial begin
    //data_mem_mdl = '{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31};
    data_mem_mdl = '{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,-16,-15,-14,-13,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1};
    //data_mem_mdl = '{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
end

// Pointers to current instruction and address in program memory
logic [31:0] curr_inst_addr;
logic [31:0] curr_data_addr;

// Output ports
assign sync_rst = local_sync_rst;
assign async_rst_n = local_async_rst_n;
assign uart_rx = local_uart_rx;


//Clock generation 
initial begin
    local_clk = 1'b1;
    forever
        #25 local_clk = ~local_clk; // 20 MHz
end
assign clk = local_clk;

// Tasks and functions to be included
`include "tb_tasks_inc.sv"

// Async reset DUT
task async_rst_n_dut();
    #`ONE_CLK_CYCLE 
    local_async_rst_n = 1;
    
    #`ONE_CLK_CYCLE
    local_async_rst_n = 0;
    
    #`ONE_CLK_CYCLE 
    local_async_rst_n = 1;     
endtask
assign async_rst_n = local_async_rst_n;

// sync reset DUT
task sync_rst_dut();
    #`ONE_CLK_CYCLE 
    local_sync_rst = 0;
    
    #`ONE_CLK_CYCLE
    local_sync_rst = 1;
    
    #`ONE_CLK_CYCLE 
    local_sync_rst = 0;     
endtask
assign sync_rst = local_sync_rst;

// Driver for UART RX dut port
task uart_rx_driver(input logic [7:0] data);
    integer ii;
    
    local_uart_rx = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    #`ONE_SERIAL_BIT_CYCLE;
    
    //start bit
    local_uart_rx = 0;
    #`ONE_SERIAL_BIT_CYCLE;
    
    // data bits
    for (ii=0; ii<8; ii=ii+1) begin
        local_uart_rx <= data[ii];
        #`ONE_SERIAL_BIT_CYCLE;
    end
    
    //stop bit
    local_uart_rx = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    
endtask

// Program interface
task write_to_pmem(input logic [`ALEN-1:0] addr, input [3:0] mode, input logic [`ALEN-1:0] len, input logic [31:0] data );

    logic [31:0] addr_local;
    logic [27:0] len_local;
    logic [3:0]  mode_local;
    
    addr_local = addr;
    len_local = len;
    mode_local = mode;
    
    // Start key (triggers start_programming_rd, and PMEM reset)   
    uart_rx_driver(8'hFF);
    uart_rx_driver(8'h00);
    uart_rx_driver(8'hFF);
    uart_rx_driver(8'h00);
    
    // Length
    uart_rx_driver(len_local[7:0]);
    uart_rx_driver(len_local[15:8]);
    uart_rx_driver(len_local[23:16]);
    uart_rx_driver({mode_local, len_local[27:24]});   
    
    // Address
    uart_rx_driver(addr_local[7:0]);
    uart_rx_driver(addr_local[15:8]);
    uart_rx_driver(addr_local[23:16]);
    uart_rx_driver(addr_local[31:24]); 
    
    // The actual data      
    uart_rx_driver(data[7:0]);
    uart_rx_driver(data[15:8]);
    uart_rx_driver(data[23:16]);
    uart_rx_driver(data[31:24]);
       
endtask

task program_if(input logic [`ALEN-1:0] addr, input logic [31:0] data );
    write_to_pmem(addr, 2, 1, data );
endtask

task reset_program_mem();
    // Start key (triggers start_programming_rd, and PMEM reset)   
    uart_rx_driver(8'hFF);
    uart_rx_driver(8'h00);
    uart_rx_driver(8'hFF);
    uart_rx_driver(8'h00);
    
    // Length
    uart_rx_driver(0);
    uart_rx_driver(0);
    uart_rx_driver(0);
    uart_rx_driver({1, 4'b0});   
endtask

task reset_core();
    sync_rst_dut();
endtask

// Main sequance
initial begin
    
    seed = 0;
    
    is_pass = 0;
    
    local_interrupt_in = 8'b0;
    
    curr_inst_addr = 32'h0;
    curr_data_addr = 32'h3ffff;

    #`ONE_CLK_CYCLE 
    async_rst_n_dut();
    
    #`ONE_CLK_CYCLE
    #`ONE_CLK_CYCLE 
    #`ONE_CLK_CYCLE 
    #`ONE_CLK_CYCLE 
    #`ONE_CLK_CYCLE 
    #`ONE_CLK_CYCLE 
    #`ONE_CLK_CYCLE  
    sync_rst_dut();
    
    #`ONE_CLK_CYCLE 
    check_all_instructions(seed);
        
    #`ONE_CLK_CYCLE
    #`ONE_CLK_CYCLE
    #`ONE_CLK_CYCLE
    is_pass = 1;
    $finish;  
    
end
assign interrupt_in = local_interrupt_in;

// Wrapping Up
final begin
    if ( is_pass == 1 )
        $display("FINSISHED SUCCESSFULLY!!!");
    else begin
        $display("CHECK MISMATCH :(, seed = %d", seed );
    end

end
endprogram
    