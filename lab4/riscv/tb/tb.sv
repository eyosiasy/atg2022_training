`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Comnics
// Engineer: Eyosias Yoseph Imana
// 
// Create Date: 01/06/2020 11:50:23 PM
// Design Name: soco
// Module Name: soco_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: Nefse yemitwedih ante wede, esti likegneleh enes kelibe
//                      anten anten asere telegnalech, nefse bante fikir teyizalech
//////////////////////////////////////////////////////////////////////////////////


module tb;

logic                   clk;
logic                   sync_rst;
logic                   async_rst_n;

// UART
logic                   uart_tx;
logic                   uart_rx;

// Others
logic   [31:0]          port_in;
logic   [31:0]          port_out;
logic   [7:0]           interrupt_in;
logic                   loading_program;


// DUT
top dut ( .* );

// TB Environment 
tb_env env ( .* );

endmodule
