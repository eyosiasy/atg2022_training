module sha256_hash_block ( 
	
   input logic [31:0] a, b, c, d, e, f, g, h, w, 
   input logic [5:0] t,
   output logic [255:0] hash
);
	
	function logic [31:0] Kvalue( input logic [5:0] idx );

		case ( idx ) 
			6'h00: Kvalue = 32'h428a2f98; 
			6'h01: Kvalue = 32'h71374491; 
			6'h02: Kvalue = 32'hb5c0fbcf; 
			6'h03: Kvalue = 32'he9b5dba5;
			6'h04: Kvalue = 32'h3956c25b; 
			6'h05: Kvalue = 32'h59f111f1; 
			6'h06: Kvalue = 32'h923f82a4; 
			6'h07: Kvalue = 32'hab1c5ed5; 
			6'h08: Kvalue = 32'hd807aa98;
			6'h09: Kvalue = 32'h12835b01; 
			6'h0A: Kvalue = 32'h243185be; 
			6'h0B: Kvalue = 32'h550c7dc3; 
			6'h0C: Kvalue = 32'h72be5d74; 
			6'h0D: Kvalue = 32'h80deb1fe;
			6'h0E: Kvalue = 32'h9bdc06a7; 
			6'h0F: Kvalue = 32'hc19bf174; 
			6'h10: Kvalue = 32'he49b69c1; 
			6'h11: Kvalue = 32'hefbe4786; 
			6'h12: Kvalue = 32'h0fc19dc6;
			6'h13: Kvalue = 32'h240ca1cc; 
			6'h14: Kvalue = 32'h2de92c6f; 
			6'h15: Kvalue = 32'h4a7484aa; 
			6'h16: Kvalue = 32'h5cb0a9dc; 
			6'h17: Kvalue = 32'h76f988da;
			6'h18: Kvalue = 32'h983e5152; 
			6'h19: Kvalue = 32'ha831c66d; 
			6'h1A: Kvalue = 32'hb00327c8; 
			6'h1B: Kvalue = 32'hbf597fc7; 
			6'h1C: Kvalue = 32'hc6e00bf3;
			6'h1D: Kvalue = 32'hd5a79147; 
			6'h1E: Kvalue = 32'h06ca6351; 
			6'h1F: Kvalue = 32'h14292967; 
			6'h20: Kvalue = 32'h27b70a85; 
			6'h21: Kvalue = 32'h2e1b2138;
			6'h22: Kvalue = 32'h4d2c6dfc; 
			6'h23: Kvalue = 32'h53380d13; 
			6'h24: Kvalue = 32'h650a7354; 
			6'h25: Kvalue = 32'h766a0abb; 
			6'h26: Kvalue = 32'h81c2c92e;
			6'h27: Kvalue = 32'h92722c85; 
			6'h28: Kvalue = 32'ha2bfe8a1; 
			6'h29: Kvalue = 32'ha81a664b; 
			6'h2A: Kvalue = 32'hc24b8b70; 
			6'h2B: Kvalue = 32'hc76c51a3;
			6'h2C: Kvalue = 32'hd192e819; 
			6'h2D: Kvalue = 32'hd6990624; 
			6'h2E: Kvalue = 32'hf40e3585; 
			6'h2F: Kvalue = 32'h106aa070; 
			6'h30: Kvalue = 32'h19a4c116;
			6'h31: Kvalue = 32'h1e376c08; 
			6'h32: Kvalue = 32'h2748774c; 
			6'h33: Kvalue = 32'h34b0bcb5; 
			6'h34: Kvalue = 32'h391c0cb3; 
			6'h35: Kvalue = 32'h4ed8aa4a;
			6'h36: Kvalue = 32'h5b9cca4f; 
			6'h37: Kvalue = 32'h682e6ff3; 
			6'h38: Kvalue = 32'h748f82ee; 
			6'h39: Kvalue = 32'h78a5636f; 
			6'h3A: Kvalue = 32'h84c87814;
			6'h3B: Kvalue = 32'h8cc70208; 
			6'h3C: Kvalue = 32'h90befffa; 
			6'h3D: Kvalue = 32'ha4506ceb; 
			6'h3E: Kvalue = 32'hbef9a3f7; 
			6'h3F: Kvalue = 32'hc67178f2;
			
			default: Kvalue = 32'h428a2f98;
		endcase
	endfunction

	function logic [255:0] hash_out(input logic [31:0] a, b, c, d, e, f, g, h, w, 
                                   input logic [5:0] t); 
										  
		logic [31:0] K, s0, s1, maj, ch, t1, t2, aNew, eNew;
		
		// Add your design here
	
	endfunction


	assign hash = hash_out( a, b, c, d, e, f, g, h, w, t);	

endmodule	