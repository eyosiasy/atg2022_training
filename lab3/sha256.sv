module sha256(
	input 	logic 			clk, 
	input 	logic 			reset_n, 
	input 	logic 			start,
	input 	logic [31:0] 	message_addr, 
	input 	logic [31:0] 	size, 
	input	logic [31:0]	output_addr,
	
	output 	logic 			done,
	output	logic			mem_clk, 
	output	logic			mem_we,
	output 	logic [15:0] 	mem_addr,
	output 	logic [31:0] 	mem_write_data,
	
	input 	logic [31:0] 	mem_read_data
);

	enum logic [1:0] { 
		IDLE  = 2'b00, 
		INIT  = 2'b01,
		RUN   = 2'b10, 
		WRITE = 2'b11 
	} state;

	
	localparam INIT_HASH = { 
		32'h6a09e667,
		32'hbb67ae85,
		32'h3c6ef372,
		32'ha54ff53a,
		32'h510e527f,
		32'h9b05688c,
		32'h1f83d9ab,
		32'h5be0cd19
	};
	
	logic [15:0] num_of_blks;
	logic [31:0] total_size_in_words;
	logic [63:0] total_size_in_bits;
	logic [31:0] num_of_msg_words;
	logic [1:0]  num_of_residue_bytes;
	logic [31:0] size_at_strt;
	logic [15:0] output_addr_at_strt;
	
	logic [15:0] msg_word_cntr;
	logic [5:0] word_cntr;
	logic [9:0] blk_cntr;
	
	logic [31:0] a, b, c, d, e, f, g, h;
	logic [31:0] h0, h1, h2, h3, h4, h5, h6, h7;
	logic [31:0] w[17];
	logic [255:0] hash;
	
	logic [31:0] unscrambled_word;
	logic [31:0] mem_read_data_mod;
	
	//---------------------------------------------------------------------
	// left rotate function, this is used to compute the unscrambled word 
	// when the word_cntr is between 16 and 63 inclusively 
	//---------------------------------------------------------------------
	function logic [31:0] rightrotate(input logic [31:0] w1, w2, w3, w4); 
		logic [31:0] s0, s1;
		
		s0 = {w2[06:00],w2[31:07]} ^ 
			  {w2[17:00],w2[31:18]} ^ 
			  (w2 >> 3);
			  
		s1 = {w4[16:00],w4[31:17]} ^ 
			  {w4[18:00],w4[31:19]} ^ 
			  (w4 >> 10);
			  
		rightrotate = w1 + s0 + w3 + s1;
	endfunction
	
	// change Endian
	function logic [31:0] changeEndian(input logic [31:0] value);
    changeEndian = {value[7:0], value[15:8], value[23:16], value[31:24]};
	endfunction
	
	//sizing wires
	
	// ------------------------------------------------------------------------
	// This is the number of message words including the last message word which 
	// may contain less than 4 bytes. 
	// -------------------------------------------------------------------------
	assign num_of_msg_words = ( size[1:0] == 2'b0 ) ? {2'b0, size[31:2]} : 
																	  {2'b0, size[31:2]} + 32'b1;
																	  
	// ------------------------------------------------------------------------
	// The number of message bytes in the last message word
	// If the size is muliple of 4, the last two bits (LSBs) will be zero
	// otherwise, the last two bits are equal to the number of residue bytes
	// -------------------------------------------------------------------------																  
	assign num_of_residue_bytes =  size[1:0]; //size % 4
	
	
	assign total_size_in_words =  {16'b0,num_of_blks[11:0], 4'b0}; // num_of_blocks*16
	
	//-------------------------------------------------------------------------
	// num_of_blocks*64*8
	// This is what is put at the last two words put to unscrambled_word
	//------------------------------------------------------------------------
	assign total_size_in_bits =  {32'b0, size[28:0], 3'b0}; // num_of_blocks*64*8
	

	//FSM 
	always_ff @ ( posedge clk or negedge reset_n ) begin
	
		if ( !reset_n ) begin
			state <= IDLE;
		end 
		else begin 
			case ( state ) 
				IDLE:
					if ( start ) begin
					
						size_at_strt <= size;
						output_addr_at_strt <= output_addr[15:0];
					
						mem_addr <= message_addr[15:0];
						
						state <= INIT;
					end
					
				INIT: begin
					word_cntr <= 0;
					blk_cntr <= 0;
					mem_addr <= mem_addr[15:0] + 16'b1;
					
					{a, b, c, d, e, f, g, h} <= INIT_HASH;
					{h0, h1, h2, h3, h4, h5, h6, h7} <= INIT_HASH;
					
					state <= RUN;
				end
				
				RUN: begin
		
					if ( word_cntr <= 14 ) begin
						mem_addr <= mem_addr[15:0] + 16'b1;
					end
				
					if ( word_cntr < 63 ) begin
						word_cntr <= word_cntr + 8'b1;
						
						{a, b, c, d, e, f, g, h} = hash;	
					end
					else if ( word_cntr == 63 ) begin
						word_cntr <= 0;	
						blk_cntr <= blk_cntr + 10'b1;
						mem_addr <= mem_addr[15:0] + 16'b1;
						
						a <= hash[255:224] + h0;
						b <= hash[223:192] + h1;
						c <= hash[191:160] + h2;
						d <= hash[159:128] + h3;
						e <= hash[127:096] + h4;
						f <= hash[095:064] + h5;
						g <= hash[063:032] + h6;
						h <= hash[031:000] + h7;
						
						h0 <= hash[255:224] + h0;
						h1 <= hash[223:192] + h1;
						h2 <= hash[191:160] + h2;
						h3 <= hash[159:128] + h3;
						h4 <= hash[127:096] + h4;
						h5 <= hash[095:064] + h5;
						h6 <= hash[063:032] + h6;
						h7 <= hash[031:000] + h7;
						
						if ( blk_cntr == num_of_blks - 1 ) begin
							state <= WRITE;
						end
						
					end			
				end
			
				WRITE: begin
				
					word_cntr <= word_cntr + 8'b1;
					
					if ( word_cntr == 0 ) begin 
						mem_write_data <= h0;
						mem_addr <= output_addr_at_strt;
						mem_we <= 1'b1;
					end	
					else if ( word_cntr == 1 ) begin
						mem_write_data <= h1;
						mem_addr <= mem_addr[15:0] + 16'b1;
						mem_we <= 1'b1;
					end
					else if ( word_cntr == 2 ) begin
						mem_write_data <= h2;
						mem_addr <= mem_addr[15:0] + 16'b1;
						mem_we <= 1'b1;
					end
					else if ( word_cntr == 3 ) begin
						mem_write_data <= h3;
						mem_addr <= mem_addr[15:0] + 16'b1;	
					   mem_we <= 1'b1;	
					end
					else if ( word_cntr == 4 ) begin
						mem_write_data <= h4;
						mem_addr <= mem_addr[15:0] + 16'b1;	
						mem_we <= 1'b1;

					end
					else if ( word_cntr == 5 ) begin
						mem_write_data <= h5;
						mem_addr <= mem_addr[15:0] + 16'b1;	
						mem_we <= 1'b1;

					end
					else if ( word_cntr == 6 ) begin
						mem_write_data <= h6;
						mem_addr <= mem_addr[15:0] + 16'b1;	
						mem_we <= 1'b1;

					end
					else if ( word_cntr == 7 ) begin
						mem_write_data <= h7;
						mem_addr <= mem_addr[15:0] + 16'b1;	
						mem_we <= 1'b1;

					end
					else if ( word_cntr == 10 ) begin
   					mem_we <= 1'b0;
					
						state <= IDLE;
					end
				
				end
				
			endcase
				
		end//else of if ( !reset_n ) begin
	end//always_ff

	
	assign mem_clk = clk;
	
	assign done = ( state == WRITE ) & ( word_cntr == 10 ) ;
	assign msg_word_cntr = mem_addr - message_addr[15:0];
	
	// mux selector for memory output
	assign mem_read_data_mod = changeEndian( mem_read_data );
	
	//-----------------------------------------------------------------------
	// msg_word_cntr is the count of 32 bit words in the message blocks
	// num_of_msg_words is the number of message words (without including
	// delimiter 1, zero padding, and length words)
	// 
	// this mux supports cases where the last message word is not neccessarily
	// 32 bits wide. Hence, some of the bytes containing the last message byte
	// can be occupied  by 1 delimiter, and zero paddings. 
	//
	// num_of_residue_bytes tells how many message bytes are occupying the last 
	// message word. 
	// 
	// if the num_of_residue_bytes is zero, the 1 delimiter is padded in the next
	// word
	//
	// The very last two words (64 bits) correspond to the length information
	//---------------------------------------------------------------------
	
	always_comb begin
		if ( msg_word_cntr < num_of_msg_words ) 
			unscrambled_word = mem_read_data_mod;
		else if ( msg_word_cntr == num_of_msg_words & num_of_residue_bytes == 0 ) 
			unscrambled_word = mem_read_data_mod;	
		else if ( msg_word_cntr == num_of_msg_words & num_of_residue_bytes == 1 )
			unscrambled_word = ( mem_read_data_mod & 32'hFF000000 ) | 32'h00800000;
		else if ( msg_word_cntr == num_of_msg_words & num_of_residue_bytes == 2 )
			unscrambled_word = ( mem_read_data_mod & 32'hFFFF0000 ) | 32'h00008000;
		else if ( msg_word_cntr == num_of_msg_words & num_of_residue_bytes == 3 )
			unscrambled_word = ( mem_read_data_mod & 32'hFFFFFF00 ) | 32'h00000080; 
		else if ( msg_word_cntr == num_of_msg_words + 1 & num_of_residue_bytes == 0 ) 
			unscrambled_word = 32'h80000000;	
		else if ( msg_word_cntr >= num_of_msg_words + 1 & msg_word_cntr + 1 < total_size_in_words ) 
			unscrambled_word = 32'h00000000;		
		else if ( msg_word_cntr > num_of_msg_words + 1 & msg_word_cntr + 1 == total_size_in_words )
			unscrambled_word = total_size_in_bits[63:32];
		else //( msg_word_cntr > num_of_msg_words + 1 & msg_word_cntr == total_size_in_words + 1 )
			unscrambled_word = total_size_in_bits[31:0];
	end
	
	// ----------------------------------------------------------------------------
	// when word_cntr is less than 16 (0 --> 15) then, the unscrambled word is 
	// either a word from the message, or 0 padding added to the message or the 
	// the length of the message in bits. 
	
	// For word_cntr >= 16 (16-->63), the unscrambled word is computed from previous
	// unscrambed words using the formula that is speciefied in the algorithm. 
	// ----------------------------------------------------------------------------
	always_comb begin
		if ( word_cntr < 16 ) 
			w[0] = unscrambled_word;
		else 
			w[0] = rightrotate( w[16], w[15], w[7], w[2] );
	end

	// ---------------------------------------------------------------------
	// This creates a delay line for w to establish memory for future
	// access
	// ---------------------------------------------------------------------
	genvar idx;
	generate 
	for ( idx = 1; idx <= 16 ; idx = idx + 1 ) begin: delay_line
		always_ff	@ ( posedge clk ) 
			if ( state == RUN ) 
				w[idx] <= w[idx - 1];
	end
	endgenerate
	
	// calculate sizes
	calc_num_blocks num_blks( 
		.size(size_at_strt),
		.num_blocks(num_of_blks)
	);
	
	
	// calculate next hash
	sha256_hash_block hasher( 
		.a(a), 
		.b(b), 
		.c(c), 
		.d(d),
		.e(e),
		.f(f),
		.g(g),
		.h(h),
		.w(w[0]), 
		.t(word_cntr),
		.hash(hash)
	);

	

	
endmodule
	