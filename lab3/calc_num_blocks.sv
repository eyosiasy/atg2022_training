`timescale 1ns / 1ps
module calc_num_blocks( 

	input logic [31:0] size,
	output logic [15:0] num_blocks
	
	);
	
	
function logic [15:0] determine_num_blocks( input logic [31:0] size );
	
	
	automatic logic [34:0] size_in_bits = size << 3;
	
	determine_num_blocks = ( ( size_in_bits + 34'h40 ) >> 9 ) + 1; 
	
endfunction

assign num_blocks = determine_num_blocks(size);
	
endmodule