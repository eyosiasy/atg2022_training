`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2022 03:35:30 AM
// Design Name: 
// Module Name: uart_bridge_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`define ONE_CLK_CYCLE 50
`define ONE_SERIAL_BIT_CYCLE 8681
`define TEST_DATA_LEN 4

module uart_bridge_tb;

logic                        is_pass;

logic                        clk;
logic                        sync_rst;
   
// UART
logic                       uart_tx_seral_data_out;
logic                       uart_rx_seral_data_in;

// Memory Rd/Wr Interface
logic                       memb_wr_en;
logic   [9:0]               memb_addr;
logic   [31:0]              memb_wr_data;
logic   [31:0]              memb_rd_data;


// DUT
uart_bridge u_dut( .* );

// Clock gen
logic local_clk;
initial begin
    local_clk = 1'b1;
    forever
        #25 local_clk = ~local_clk; // 20 MHz
end
assign clk = local_clk;

// Serail TX interface
logic local_uart_rx_seral_data_in;

task serial_tx_if(input logic [7:0] data);
    integer ii;
    
    local_uart_rx_seral_data_in = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    #`ONE_SERIAL_BIT_CYCLE;
    
    //start bit
    local_uart_rx_seral_data_in = 0;
    #`ONE_SERIAL_BIT_CYCLE;
    
    // data bits
    for (ii=0; ii<8; ii=ii+1) begin
        local_uart_rx_seral_data_in <= data[ii];
        #`ONE_SERIAL_BIT_CYCLE;
    end
    
    //stop bit
    local_uart_rx_seral_data_in = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    
endtask
assign uart_rx_seral_data_in = local_uart_rx_seral_data_in;

// Reset program memory
task reset_mem();

       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(8'h00); 
       serial_tx_if(8'h00); 
       serial_tx_if(8'h00);
       serial_tx_if(8'h80); //resetting
    
endtask

// Read from memory
logic [7:0] uart_tx_data_out;
logic uart_tx_data_out_vld;

task mem_read(input logic [9:0] start_addr, input [23:0] len );
       integer ii;
 
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(len[7:0]); 
       serial_tx_if(len[15:8]); 
       serial_tx_if(len[23:16]);
       serial_tx_if(8'h40); //reading mode
       serial_tx_if(start_addr[7:0]);
       serial_tx_if({6'b0,start_addr[9:8]});
       serial_tx_if(8'h00);
       serial_tx_if(8'h00); 
       
endtask

//RX
uart_rx #(.CLKS_PER_BIT(174)) my_uart_rx
  (.i_Clock(clk),
   .i_Rx_Serial(uart_tx_seral_data_out),
   .o_Rx_DV(uart_tx_data_out_vld),
   .o_Rx_Byte(uart_tx_data_out)
   );

// Write to memory
task mem_write(input logic [9:0] start_addr, input logic [31:0] data[4], input [23:0] len );
       integer ii;
 
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(len[7:0]); 
       serial_tx_if(len[15:8]); 
       serial_tx_if(len[23:16]);
       serial_tx_if(8'h00); //writing mode
       serial_tx_if(start_addr[7:0]);
       serial_tx_if({6'b0,start_addr[9:8]});
       serial_tx_if(8'h00);
       serial_tx_if(8'h00); 
       
       for (ii=0; ii < len ; ii=ii+1 ) begin     
            serial_tx_if(data[ii][7:0]);
            serial_tx_if(data[ii][15:8]);
            serial_tx_if(data[ii][23:16]);
            serial_tx_if(data[ii][31:24]);
       end
       
endtask

// Test Data
logic [31:0] test_data[`TEST_DATA_LEN], mem_init_coe[2], mem_rd_out[`TEST_DATA_LEN];
initial begin
    test_data = '{45,46,848488484,125};
    mem_init_coe = '{32'h000480b7, 32'h6a008093};
end

// Main test sequence
integer ii,jj;
logic [7:0] exp_byte;
initial begin
   sync_rst = 0;
   is_pass = 0;
   #(`ONE_CLK_CYCLE * 10)
   
   // Reset
   sync_rst = 1;
   # `ONE_CLK_CYCLE sync_rst = 0;
   #(`ONE_CLK_CYCLE * 10)
   
   // Read from Memory
   mem_read(0,2);
   
   // Check Reading 
   for(ii=0;ii<2;ii=ii+1) begin
        for(jj=0;jj<4;jj=jj+1) begin
            @(posedge clk & uart_tx_data_out_vld==1)
            //if ( uart_tx_data_out_vld ) begin
            exp_byte = ( mem_init_coe[ii] >>  (jj*8) );
            $display("Reading: Sample %d, byte %d, act = %d, exp = %d",  ii, jj, uart_tx_data_out,exp_byte);
            #(`ONE_CLK_CYCLE*3)
            if ( uart_tx_data_out !== exp_byte ) begin
                is_pass = 0;
                $finish;            
            end
            
            //end
        end   
   end
   
   // Reset
   #(`ONE_CLK_CYCLE * 10)
   reset_mem();
   #(`ONE_CLK_CYCLE * 10)
   
   //  Write to memory
   mem_write(0, test_data, `TEST_DATA_LEN );
   #(`ONE_CLK_CYCLE * 100)
    
   // Read memory 
   for (ii=0; ii < `TEST_DATA_LEN ; ii=ii+1 ) begin
      memb_addr = ii;
      #(`ONE_CLK_CYCLE * 2)  
      $display("Writing1: Sample %d, act = %d, exp = %d",  ii, memb_rd_data,test_data[ii]);
      if ( memb_rd_data != test_data[ii] ) begin
            is_pass = 0;
            $finish;
      end
      mem_rd_out[ii] = memb_rd_data;
   end
   
   // Reset memory
   #(`ONE_CLK_CYCLE * 10) 
   reset_mem();
   
   // Mem write
   #(`ONE_CLK_CYCLE * 10) 
   mem_write(0, test_data, 1 );
   #(`ONE_CLK_CYCLE * 100)   
   
   memb_addr = 0;
   #(`ONE_CLK_CYCLE * 2)
   $display("Writing2: Sample %d, act = %d, exp = %d",  0, memb_rd_data,test_data[0]);  
   if ( memb_rd_data !== test_data[0] ) begin
       is_pass = 0;
       $finish;
   end   
   
   is_pass = 1;
   $finish;
   
   
end

final begin
    if ( is_pass == 1 )
        $display("FINSISHED SUCCESSFULLY!!!");
    else begin
        $display("CHECK MISMATCH :(");
    end
end

endmodule
