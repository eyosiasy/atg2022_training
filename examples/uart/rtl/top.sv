`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2022 07:43:17 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
 

module top( 
    input   clk_board,
    output  clk_out,
    input   en,    
    
    output  uart_tx,
    input   uart_rx, 
    
    input  [3:0] sw,
    output [3:0] led
    );

logic           clk;
logic           sync_rst;
(* mark_debug = "true" *) logic           memb_wr_en;
(* mark_debug = "true" *) logic [9:0]     memb_addr;
(* mark_debug = "true" *) logic [31:0]    memb_wr_data;
(* mark_debug = "true" *) logic [31:0]    memb_rd_data;
logic           pll_locked;
logic [19:0]    pll_locked_dline;

// Bridge   
uart_bridge u_uart_bridge(
    .uart_tx_seral_data_out(uart_tx),
    .uart_rx_seral_data_in(uart_rx),
    .*
    );
    
// PLL
clk_wiz_0 u_clock
   (
    // Clock out ports
    .clk_out1(clk),     // output clk_out1
    // Status and control signals
    .reset(~en), // input reset
    .locked(pll_locked),       // output locked
   // Clock in ports
    .clk_in1(clk_board));   

// Generate sync reset
always @ ( posedge clk )
    if ( ~pll_locked )
        pll_locked_dline <= 0;
    else
        pll_locked_dline <= {pll_locked_dline[18:0], pll_locked};  
        
assign sync_rst = ~pll_locked_dline[18] & pll_locked_dline[17];

// Debug
assign clk_out = clk;

(* mark_debug = "true" *) logic [9:0] clk_cntr;
localparam [9:0] DEBUG_MEM_RD_ADDR = 0;
localparam [9:0] DEBUG_MEM_WR_ADDR = 3;
(* mark_debug = "true" *) logic [3:0] local_led;
always_ff @ ( posedge clk )
    if ( sync_rst ) begin
        clk_cntr <= 0;
        memb_addr <= 0;
        memb_wr_en <= 0;
    end
    else begin
        clk_cntr <= clk_cntr + 1;
        memb_wr_en <= 0;
        if ( clk_cntr == 900 ) begin
            memb_addr <= DEBUG_MEM_WR_ADDR; 
            memb_wr_en <= 1;
            memb_wr_data <= {28'b0,sw};
        end
        else begin 
            memb_wr_en <= 0;   
            memb_wr_data <= 0;    
            if ( clk_cntr == 1000 )
                memb_addr <= DEBUG_MEM_RD_ADDR;
        end
        
        if ( clk_cntr == 1003 )
            local_led <= memb_rd_data[3:0];
    end    
assign led = local_led;

endmodule
