`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2022 08:15:19 PM
// Design Name: 
// Module Name: top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`define ONE_CLK_CYCLE 10
`define ONE_SERIAL_BIT_CYCLE 8681

module top_tb;

logic           is_pass;

logic           clk_board;
logic           clk_out;
logic           en;
   
// UART
logic           uart_tx;
logic           uart_rx;

// Debug ports
logic   [3:0]   led;
logic   [3:0]   sw;

// DUT
top u_dut( .* );


// Clock Gen
logic local_clk_board;
initial begin
    local_clk_board = 1'b1;
    forever
        #5 local_clk_board = ~local_clk_board; //100 MHz
end
assign clk_board = local_clk_board;

// Serail TX interface
logic local_uart_rx_seral_data_in;

task serial_tx_if(input logic [7:0] data);
    integer ii;
    
    local_uart_rx_seral_data_in = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    #`ONE_SERIAL_BIT_CYCLE;
    
    //start bit
    local_uart_rx_seral_data_in = 0;
    #`ONE_SERIAL_BIT_CYCLE;
    
    // data bits
    for (ii=0; ii<8; ii=ii+1) begin
        local_uart_rx_seral_data_in <= data[ii];
        #`ONE_SERIAL_BIT_CYCLE;
    end
    
    //stop bit
    local_uart_rx_seral_data_in = 1;
    #`ONE_SERIAL_BIT_CYCLE;
    
endtask
assign uart_rx = local_uart_rx_seral_data_in;

// Reset program memory
task reset_mem();

       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(8'h00); 
       serial_tx_if(8'h00); 
       serial_tx_if(8'h00);
       serial_tx_if(8'h80); //resetting
    
endtask

// Read from memory
logic [7:0] uart_tx_data_out;
logic uart_tx_data_out_vld;

task mem_read(input logic [9:0] start_addr, input [23:0] len );
       integer ii;
 
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(len[7:0]); 
       serial_tx_if(len[15:8]); 
       serial_tx_if(len[23:16]);
       serial_tx_if(8'h40); //reading mode
       serial_tx_if(start_addr[7:0]);
       serial_tx_if({6'b0,start_addr[9:8]});
       serial_tx_if(8'h00);
       serial_tx_if(8'h00); 
       
endtask

//RX
uart_rx #(.CLKS_PER_BIT(174)) my_uart_rx
  (.i_Clock(clk_out),
   .i_Rx_Serial(uart_tx),
   .o_Rx_DV(uart_tx_data_out_vld),
   .o_Rx_Byte(uart_tx_data_out)
   );

// Write to memory
task mem_write(input logic [9:0] start_addr, input logic [31:0] data[4], input [23:0] len );
       integer ii;
 
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF);
       serial_tx_if(8'h00);
       serial_tx_if(8'hFF); 
       serial_tx_if(len[7:0]); 
       serial_tx_if(len[15:8]); 
       serial_tx_if(len[23:16]);
       serial_tx_if(8'h00); //writing mode
       serial_tx_if(start_addr[7:0]);
       serial_tx_if({6'b0,start_addr[9:8]});
       serial_tx_if(8'h00);
       serial_tx_if(8'h00); 
       
       for (ii=0; ii < len ; ii=ii+1 ) begin     
            serial_tx_if(data[ii][7:0]);
            serial_tx_if(data[ii][15:8]);
            serial_tx_if(data[ii][23:16]);
            serial_tx_if(data[ii][31:24]);
       end
       
endtask

// Test Data
logic [31:0] test_data[4], mem_init_coe[2];
initial begin
    test_data = '{45,46,848488484,125};
    mem_init_coe = '{32'h000480b7, 32'h6a008093};
end

// Main test sequence
integer ii,jj;
logic [7:0] exp_byte;
initial begin
   sw = 4'b1010;
   en = 0;
   is_pass = 0;
   #(`ONE_CLK_CYCLE * 10)
   
   // Reset
   en = 1;
   #(`ONE_CLK_CYCLE * 10)
   
   // Read from Memory
   mem_read(3,1);
   
   // Check Reading 
   @(posedge clk_out & uart_tx_data_out_vld==1)
   $display("SW: act = %d, exp = %d", uart_tx_data_out,sw);
   #(`ONE_CLK_CYCLE*3)
   if ( uart_tx_data_out !== sw ) begin
        is_pass = 0;
        $finish;            
   end

   //  Write to memory
   mem_write(0, test_data, 1 );
   #(`ONE_CLK_CYCLE * 1000 * 5)
    
   $display("LED: act = %d, exp = %d", led,test_data[0][3:0]);
   if ( led !== test_data[0][3:0] ) begin
        is_pass = 0;
        $finish;
   end   
   
   is_pass = 1;
   $finish;
   
   
end

final begin
    if ( is_pass == 1 )
        $display("FINSISHED SUCCESSFULLY!!!");
    else begin
        $display("CHECK MISMATCH :(");
    end
end

endmodule