`timescale 1ns / 1ps

`define MEM_DEPTH 10 //2^MEM_DEPTH
`define BRIDGE_STATE_ADDR 1024
`define UART_START_WORD 32'hFF00FF00

module uart_bridge (

   input                        clk,
   input                        sync_rst,
   
   // UART
   output                       uart_tx_seral_data_out,
   input                        uart_rx_seral_data_in,

   // Memory Rd/Wr Interface
   input                       memb_wr_en,
   input   [`MEM_DEPTH-1:0]    memb_addr,
   input   [31:0]              memb_wr_data,
   output  [31:0]              memb_rd_data
);

//
(* mark_debug = "true" *) enum logic [2:0] { 
    IDLE  = 3'b000, 
    GET_MODE_AND_LEN  = 3'b001,
    GET_ADDR = 3'b010,
    WRITE   = 3'b011, 
    READ = 3'b100,
    WAIT_FOR_UART = 3'b110,
    RESET = 3'b111 
} state;

(* mark_debug = "true" *) logic           uart_tx_data_in_vld;
(* mark_debug = "true" *) logic   [7:0]   uart_tx_data_in; 
(* mark_debug = "true" *) logic           uart_rx_data_out_vld;
(* mark_debug = "true" *) logic   [7:0]   uart_rx_data_out;
   
(* mark_debug = "true" *) logic           uart_tx_done;
(* mark_debug = "true" *) logic           uart_tx_active;

(* mark_debug = "true" *) logic           start; 

// Memory Rd/Wr Interface
logic                       mema_sync_rst;
logic                       mema_wr_en;
logic   [`MEM_DEPTH-1:0]    mema_addr;
logic   [31:0]              mema_wr_data;
logic   [31:0]              mema_rd_data;
   

logic [1:0]  uart_rx_data_out_cntr; 
logic [7:0]  uart_rx_data_out_dline[3]; 

(* mark_debug = "true" *) logic [31:0] uart_rx_word_out;
(* mark_debug = "true" *) logic   uart_rx_word_out_vld;

(* mark_debug = "true" *) logic [`MEM_DEPTH-1:0]  len;
(* mark_debug = "true" *) logic                   mode; // 0 --> write, 1 --> read
(* mark_debug = "true" *) logic [`MEM_DEPTH-1:0]  start_addr;
(* mark_debug = "true" *) logic [`MEM_DEPTH-1:0]  cntr;

logic        mem_read_en;      
logic        mem_read_en_d0;
logic        mem_read_en_d1;

logic [1:0]  byte_cntr;

//RX
uart_rx #(.CLKS_PER_BIT(174)) u_uart_rx
  (.i_Clock(clk),
   .i_Rx_Serial(uart_rx_seral_data_in),
   .o_Rx_DV(uart_rx_data_out_vld),
   .o_Rx_Byte(uart_rx_data_out)
   );
   
//TX
uart_tx #(.CLKS_PER_BIT(174)) u_uart_tx
  (.i_Clock(clk),
   .i_Tx_DV(uart_tx_data_in_vld),
   .i_Tx_Byte(uart_tx_data_in),
   .o_Tx_Active(uart_tx_active),
   .o_Tx_Serial(uart_tx_seral_data_out),
   .o_Tx_Done(uart_tx_done)
  ); 
  
// Counter
always_ff @ ( posedge clk ) 
    if ( sync_rst ) 
        uart_rx_data_out_cntr <= 0;
    else if ( uart_rx_data_out_vld ) 
        uart_rx_data_out_cntr <= uart_rx_data_out_cntr + 1;

// delay line
always_ff @ ( posedge clk )
    if ( sync_rst ) begin
        uart_rx_data_out_dline[0] <= 0;
        uart_rx_data_out_dline[1] <= 0;
        uart_rx_data_out_dline[2] <= 0;
    end
    else if ( uart_rx_data_out_vld ) begin
        uart_rx_data_out_dline[0] <= uart_rx_data_out;
        uart_rx_data_out_dline[1] <= uart_rx_data_out_dline[0];
        uart_rx_data_out_dline[2] <= uart_rx_data_out_dline[1];      
    end   

assign uart_rx_word_out_vld = uart_rx_data_out_vld & ( uart_rx_data_out_cntr == 3'b11 );
assign uart_rx_word_out = {uart_rx_data_out,uart_rx_data_out_dline[0],uart_rx_data_out_dline[1],uart_rx_data_out_dline[2]};
assign start = ( uart_rx_word_out == `UART_START_WORD ) & uart_rx_word_out_vld;

// MEM Transaction state machine
always_ff @ ( posedge clk )
    if ( sync_rst ) begin
        state <= IDLE;
        len <= 0;
        mode <= 0;
        start_addr <= 0;
        cntr <= 0;
        byte_cntr <= 0;
    end
    else
        case( state ) 
            IDLE: 
                if ( start ) begin
                    len <= 0;
                    mode <= 0;
                    start_addr <= 0;
                    cntr <= 0;
                    byte_cntr <= 0;
                    state <= GET_MODE_AND_LEN;
                end
                    
            GET_MODE_AND_LEN:
                if ( uart_rx_word_out_vld ) begin
                    len <= uart_rx_word_out[`MEM_DEPTH-1:0];
                    start_addr <= 0;
                    cntr <= 0;
                    mode <= uart_rx_word_out[30];
                    byte_cntr <= 0;
                    if ( uart_rx_word_out[31:30] == 2'b01 || uart_rx_word_out[31:30] == 2'b00 )
                        state <= GET_ADDR;
                    else //2'b10 or 2'b11
                        state <= RESET;
                end 
        
            GET_ADDR: 
                if ( uart_rx_word_out_vld ) begin
                    cntr <= 0;
                    start_addr <= uart_rx_word_out[`MEM_DEPTH-1:0];
                    byte_cntr <= 0;
                    if ( mode == 0 ) 
                        state <= WRITE;
                    else    
                        state <= READ;
                end
            
            RESET:
                state <= IDLE;
                    
            WRITE:
                if ( uart_rx_word_out_vld ) begin
                    cntr <= cntr + 1;
                    byte_cntr <= 0;
                    if ( cntr == len - 1 )
                        state <= IDLE; 
                end

            READ:
                if ( ~uart_tx_active ) begin
                
                    if ( cntr == len )
                        state <= IDLE;
                    else if ( cntr < len )
                        state <= WAIT_FOR_UART;
                end
            
            WAIT_FOR_UART: 
                if ( uart_tx_done ) begin
                    byte_cntr <= byte_cntr + 1;
                    state <= READ;
                    if ( byte_cntr == 3 )
                        cntr = cntr + 1;
                end  
                               
             default:
                state <= IDLE;
                            
        endcase
       
assign mem_read_en = ( state == READ ) & ~uart_tx_active;
always_ff @ ( posedge clk )
    if ( sync_rst | state ==  IDLE ) begin
        mem_read_en_d0 <= 0;
        mem_read_en_d1 <= 0;
    end
    else begin
        mem_read_en_d0 <= mem_read_en;
        mem_read_en_d1 <= mem_read_en_d0;  
    end

// UART TX driver
assign uart_tx_data_in_vld = mem_read_en_d1;
always_comb
    case( byte_cntr )
        0: uart_tx_data_in <= mema_rd_data[7:0];
        1: uart_tx_data_in <= mema_rd_data[15:8];
        2: uart_tx_data_in <= mema_rd_data[23:16];
        3: uart_tx_data_in <= mema_rd_data[31:24];
        default: uart_tx_data_in <= mema_rd_data[7:0];
    endcase

// Memory Interface
assign mema_sync_rst = ( state == RESET );
assign mema_wr_en = ( state == WRITE ) & uart_rx_word_out_vld;
assign mema_addr = cntr + start_addr;
assign mema_wr_data = uart_rx_word_out;

// Memory
blk_mem_gen_0 u_mem (
  .clka(clk),    // input wire clka
  .rsta(mema_sync_rst),    // input wire rsta
  .wea(mema_wr_en),      // input wire [0 : 0] wea
  .addra(mema_addr),  // input wire [9 : 0] addra
  .dina(mema_wr_data),    // input wire [31 : 0] dina
  .douta(mema_rd_data),  // output wire [31 : 0] douta
  
  .clkb(clk),    // input wire clkb
  .rstb(mema_sync_rst),    // input wire rstb
  .web(memb_wr_en),      // input wire [0 : 0] web
  .addrb(memb_addr),  // input wire [9 : 0] addrb
  .dinb(memb_wr_data),    // input wire [31 : 0] dinb
  .doutb(memb_rd_data)  // output wire [31 : 0] doutb
);

endmodule