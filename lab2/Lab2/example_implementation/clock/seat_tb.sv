`timescale 1ns / 1ps 

module seat_tb;

logic clk;  
logic en;
logic increament_push_button;
logic adjust_mode_push_button;
logic d1;
logic d2;
logic d3;
logic d4;
logic colon1;
logic colon2;
logic a;
logic b;
logic c;
logic d;
logic e;
logic f;
logic g;
logic dp;
logic clk_locked;

seat_top u_dut ( .* );

initial begin
    en = 0;
    #20 en = 1;
end

initial begin
    clk = 1'b1;
    forever
        #1 clk = ~clk;
end

endmodule