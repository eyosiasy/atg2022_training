`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/05/2022 11:35:31 PM
// Design Name: 
// Module Name: seat_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module seat_top(
    input           clk,
    input           en,
    
    input           increament_push_button,
    input    [2:0]  adjust_mode_push_button,
    
    output   reg    d1,
    output   reg    d2,
    output   reg    d3,
    output   reg    d4,
    output   reg    colon1,
    output   reg    colon2,
    output   reg    a,
    output   reg    b,
    output   reg    c,
    output   reg    d,
    output   reg    e,
    output   reg    f,
    output   reg    g,
    output   reg    dp,
    
    output          clk_locked
    );
    
   logic           clk_5MHz;
   logic           clk_locked_d;
   logic           rst;
   
   logic           sec_pulse;
   
   logic   [2:0]   adjust_mode;
   logic   [0:0]   increament;
   
   logic  [5:0]   sec;
   logic  [5:0]   min;
   logic  [4:0]   hour;
   logic  [2:0]   week_day;
   logic  [4:0]   month_day;
   logic  [3:0]   month;
   logic  [1:0]   four_years_cntr;
   logic  [9:0]   year;
   
   logic [6:0]  min_7segment[2];
   logic [6:0]  hour_7segment[2];
   
   logic [6:0]  display_digits[4];
   logic [2:0]  display_digit_cntr;
   logic        clk_1Hz;
   
   // Clock source 
   clk_source clk_src
   (
    .clk_out1(clk_5MHz),                
    .reset(~en),                       
    .locked(clk_locked),              
    .clk_in1(clk));                   
    
   //Se-at core
//   seat_core #( .CLK_DIVIDER(1000) ) u_core ( .* );
   seat_core #( .CLK_DIVIDER(5000000) ) u_core ( .adjust_mode(adjust_mode_push_button), .increament(increament_push_button & sec_pulse ), .* );
   
   // Convert min and hour to seven segment
   min_to_7segment  u_min_to_7seg ( .seven_segment(min_7segment), .*  ); 
   hour_to_7segment u_hour_to_7seg( .seven_segment(hour_7segment), .* );
    
   // Generate reset
   assign rst = clk_locked & ~clk_locked_d;
   always @ ( posedge clk_5MHz )
        if ( ~en )
            clk_locked_d <= 0;
        else
            clk_locked_d <= clk_locked;
   
   // Seven segment display driver
   always @ ( posedge clk_5MHz )
        if ( rst ) 
            display_digit_cntr <= 0;
        else
            display_digit_cntr <= display_digit_cntr + 1;
        
   always @ ( posedge clk_5MHz )
        if ( rst ) begin
            display_digits[0] <= 10;
            display_digits[1] <= 10;
            display_digits[2] <= 10;
            display_digits[3] <= 10;            
        end
        else if ( sec_pulse ) begin
            display_digits[0] <= hour_7segment[1];
            display_digits[1] <= hour_7segment[0];
            display_digits[2] <= min_7segment[1];
            display_digits[3] <= min_7segment[0];
        end
        
   assign colon1 = 0;
   assign colon2 = sec[0];
   
   always @ ( posedge clk_5MHz )
        case(display_digit_cntr)
            0: begin
                a <= display_digits[0][6];
                b <= display_digits[0][5];
                c <= display_digits[0][4];
                d <= display_digits[0][3];
                e <= display_digits[0][2];
                f <= display_digits[0][1];
                g <= display_digits[0][0];
                dp <= clk_1Hz;
                d1 <= 0;
                d2 <= 1;
                d3 <= 1;
                d4 <= 1;
            end
            2 : begin
                a <= display_digits[1][6];
                b <= display_digits[1][5];
                c <= display_digits[1][4];
                d <= display_digits[1][3];
                e <= display_digits[1][2];
                f <= display_digits[1][1];
                g <= display_digits[1][0];
                dp <= clk_1Hz;
                d1 <= 1;
                d2 <= 0;
                d3 <= 1;
                d4 <= 1;
            end
            4 : begin
                a <= display_digits[2][6];
                b <= display_digits[2][5];
                c <= display_digits[2][4];
                d <= display_digits[2][3];
                e <= display_digits[2][2];
                f <= display_digits[2][1];
                g <= display_digits[2][0];
                dp <= clk_1Hz;
                d1 <= 1;
                d2 <= 1;
                d3 <= 0;
                d4 <= 1;
            end
            6 : begin
                a <= display_digits[3][6];
                b <= display_digits[3][5];
                c <= display_digits[3][4];
                d <= display_digits[3][3];
                e <= display_digits[3][2];
                f <= display_digits[3][1];
                g <= display_digits[3][0];
                dp <= clk_1Hz;
                d1 <= 1;
                d2 <= 1;
                d3 <= 1;
                d4 <= 0;
            end
            default : begin
                a <= 0;
                b <= 0;
                c <= 0;
                d <= 0;
                e <= 0;
                f <= 0;
                g <= 0;
                dp <= 0;
                d1 <= 0;
                d2 <= 0;
                d3 <= 0;
                d4 <= 0;
            end
       endcase 
          
endmodule
