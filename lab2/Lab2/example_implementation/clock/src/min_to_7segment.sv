`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/28/2022 10:44:24 AM
// Design Name: 
// Module Name: min_to_7segment
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module min_to_7segment(
    input           [5:0]   min,
    output          [6:0]   seven_segment[2]
    );
logic [3:0] d0, d1;    
always_comb 
    case(min)
        0 : begin d1 <= 0;     d0 <= 0; end
        1 : begin d1 <= 0;     d0 <= 1; end
        2 : begin d1 <= 0;     d0 <= 2; end
        3 : begin d1 <= 0;     d0 <= 3; end
        4 : begin d1 <= 0;     d0 <= 4; end
        5 : begin d1 <= 0;     d0 <= 5; end
        6 : begin d1 <= 0;     d0 <= 6; end
        7 : begin d1 <= 0;     d0 <= 7; end
        8 : begin d1 <= 0;     d0 <= 8; end
        9 : begin d1 <= 0;     d0 <= 9; end
        10: begin d1 <= 1;     d0 <= 0; end
        11: begin d1 <= 1;     d0 <= 1; end
        12: begin d1 <= 1;     d0 <= 2; end
        13: begin d1 <= 1;     d0 <= 3; end
        14: begin d1 <= 1;     d0 <= 4; end
        15: begin d1 <= 1;     d0 <= 5; end
        16: begin d1 <= 1;     d0 <= 6; end
        17: begin d1 <= 1;     d0 <= 7; end
        18: begin d1 <= 1;     d0 <= 8; end
        19: begin d1 <= 1;     d0 <= 9; end
        20: begin d1 <= 2;     d0 <= 0; end
        21: begin d1 <= 2;     d0 <= 1; end
        22: begin d1 <= 2;     d0 <= 2; end
        23: begin d1 <= 2;     d0 <= 3; end
        24: begin d1 <= 2;     d0 <= 4; end
        25: begin d1 <= 2;     d0 <= 5; end
        26: begin d1 <= 2;     d0 <= 6; end
        27: begin d1 <= 2;     d0 <= 7; end
        28: begin d1 <= 2;     d0 <= 8; end
        29: begin d1 <= 2;     d0 <= 9; end
        30: begin d1 <= 3;     d0 <= 0; end
        31: begin d1 <= 3;     d0 <= 1; end
        32: begin d1 <= 3;     d0 <= 2; end
        33: begin d1 <= 3;     d0 <= 3; end
        34: begin d1 <= 3;     d0 <= 4; end
        35: begin d1 <= 3;     d0 <= 5; end
        36: begin d1 <= 3;     d0 <= 6; end
        37: begin d1 <= 3;     d0 <= 7; end
        38: begin d1 <= 3;     d0 <= 8; end
        39: begin d1 <= 3;     d0 <= 9; end
        40: begin d1 <= 4;     d0 <= 0; end
        41: begin d1 <= 4;     d0 <= 1; end
        42: begin d1 <= 4;     d0 <= 2; end
        43: begin d1 <= 4;     d0 <= 3; end
        44: begin d1 <= 4;     d0 <= 4; end
        45: begin d1 <= 4;     d0 <= 5; end
        46: begin d1 <= 4;     d0 <= 6; end
        47: begin d1 <= 4;     d0 <= 7; end
        48: begin d1 <= 4;     d0 <= 8; end
        49: begin d1 <= 4;     d0 <= 9; end
        50: begin d1 <= 5;     d0 <= 0; end
        51: begin d1 <= 5;     d0 <= 1; end
        52: begin d1 <= 5;     d0 <= 2; end
        53: begin d1 <= 5;     d0 <= 3; end
        54: begin d1 <= 5;     d0 <= 4; end
        55: begin d1 <= 5;     d0 <= 5; end
        56: begin d1 <= 5;     d0 <= 6; end
        57: begin d1 <= 5;     d0 <= 7; end
        58: begin d1 <= 5;     d0 <= 8; end
        59: begin d1 <= 5;     d0 <= 9; end
        default: begin d1 <= 10;     d0 <= 10; end
    endcase
    
decimal_digit_to_7segment u_digit0( .digit(d0), .seven_segment(seven_segment[0]) );
decimal_digit_to_7segment u_digit1( .digit(d1), .seven_segment(seven_segment[1]) );     
endmodule
