`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/28/2022 10:36:08 AM
// Design Name: 
// Module Name: decimal_digit_to_7segment
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module decimal_digit_to_7segment(
    input           [3:0]   digit,
    output  reg     [6:0]   seven_segment
    );
 
    
always_comb 
    case(digit)
        0: seven_segment <= 7'b1111110;
        1: seven_segment <= 7'b0110000;
        2: seven_segment <= 7'b1101101;
        3: seven_segment <= 7'b1111001;
        4: seven_segment <= 7'b0110011;
        5: seven_segment <= 7'b1011011;
        6: seven_segment <= 7'b1011111;
        7: seven_segment <= 7'b1110000;
        8: seven_segment <= 7'b1111111;
        9: seven_segment <= 7'b1111011;
        default: seven_segment <= 7'b1001001;
    endcase
    
endmodule
