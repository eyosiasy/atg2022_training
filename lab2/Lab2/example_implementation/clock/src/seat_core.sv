`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05/05/2022 10:32:11 PM
// Design Name: 
// Module Name: seat_core
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
module seat_core #(parameter CLK_DIVIDER = 5000000)
(
    input           clk_5MHz,
    input           rst,
    output          sec_pulse,
    output          clk_1Hz,
    
    input   [2:0]   adjust_mode,
    input           increament,
    
    output  [5:0]   sec,
    output  [5:0]   min,
    output  [5:0]   hour,
    output  [2:0]   week_day,
    output  [4:0]   month_day,
    output  [3:0]   month,
    output  [1:0]   four_years_cntr,
    output  [9:0]   year
    );

// ----------------------------------------
//   Adjust Mode | Increamented Counter
//   __________________________________
//      000      | No adjustment
//      001      | minute
//      010      | hour
//      011      | day
//      100      | month
//      101      | year
//      110      | Do nothing
//      111      | Do nothing
// ----------------------------------------

logic [31:0]    clock_tick_counter;
logic [5:0]     sec_counter;
logic [5:0]     min_counter;
logic [4:0]     hour_counter;
logic [4:0]     day_counter;
logic [3:0]     month_counter;
logic [3:0]     year_counter;
logic [1:0]     four_years_counter;

logic sec_pulse;
logic min_pulse;
logic hour_pulse;
logic day_pulse;
logic month_pulse;
logic year_pulse;   

localparam CLK_DIVIDER_MN_ONE = CLK_DIVIDER - 1;

// Generate second's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst )
        clock_tick_counter <= 0;
    else if ( clock_tick_counter < CLK_DIVIDER_MN_ONE )
        clock_tick_counter <= clock_tick_counter + 1;
    else
        clock_tick_counter <= 0;
        
assign sec_pulse = ( clock_tick_counter == CLK_DIVIDER_MN_ONE );
assign clk_1Hz = ( clock_tick_counter < (CLK_DIVIDER>>1) );

// Generate minute's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst ) 
        sec_counter <= 0;
    else if ( sec_pulse ) 
        if ( sec_counter < 59 )
            sec_counter <= sec_counter + 1;
        else
            sec_counter <= 0;
    else if ( increament & adjust_mode == 3'b001 )
        sec_counter <= sec_counter + 1;

assign sec = sec_counter;
assign min_pulse = ( sec_counter == 59 ) & sec_pulse;

// Generate hours's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst ) 
        min_counter <= 0;
    else if ( min_pulse || (increament & adjust_mode == 3'b010) ) 
        if ( min_counter < 59 )
            min_counter <= min_counter + 1;
        else
            min_counter <= 0;

assign min = min_counter;        
assign hour_pulse = ( min_counter == 59 ) & min_pulse;

// Generate days's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst )
        hour_counter <= 0;
    else if ( hour_pulse || ( increament & adjust_mode == 3'b011 ) ) 
        if ( hour_counter < 23 )
            hour_counter <= hour_counter + 1;
        else
            hour_counter <= 0;

assign hour = hour_counter + 1;         
assign day_pulse = ( hour_counter == 23 ) * hour_pulse;

// Generate month's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst ) 
        day_counter <= 0;
    else if ( day_pulse || ( increament & adjust_mode == 3'b100 ) ) 
        if  ( ( month_counter < 12 & day_counter == 29  ) || 
              ( four_years_counter <  3 & month_counter == 12 & day_counter == 4  ) ||
              ( four_years_counter == 3 & month_counter == 12 & day_counter == 5  )  )
            day_counter <= 0;
        else
            day_counter <= day_counter + 1;

assign month_day = day_counter + 1;         
assign month_pulse = ( ( month_counter < 12 & day_counter == 29  ) || 
                       ( four_years_counter <  3 & month_counter == 12 & day_counter == 4  ) ||
                       ( four_years_counter == 3 & month_counter == 12 & day_counter == 5  ) )  * day_pulse;

// Generate year's pulse
always_ff @ ( posedge clk_5MHz )
    if ( rst )
        year_counter <= 0;
    else if ( month_pulse || ( increament & adjust_mode == 3'b101)  )
        year_counter <= year_counter + 1;

assign year = year_counter + 2020; // 2020 is year zero
           
endmodule
